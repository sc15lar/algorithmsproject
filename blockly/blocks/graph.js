/**
 * @license
 * Algorithm visualizer
 *
 * Copyright 2016 University of Leeds.
 * https://www.leeds.ac.uk/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/
 
 /**
 * @fileoverview Blocks that manipulate the graph.
 * @author lukeadamroberts@gmail.com (Luke Roberts)
 **/


Blockly.Blocks['graph_node_next'] = {
	/**
   * Block for choosing the next node from a node
   * @this Blockly.Block
   */
	 updateDropDown : function(changed){			
		graph = getNodeList();
		graph = Object.keys(graph[changed]);
		nextDropDown = [];
		for(var i in graph){
			nextDropDown.push([graph[i], graph[i]]);
		}			
		this.removeInput("NEXTDROP");
	 	 this.appendDummyInput('NEXTDROP')
        .appendField(new Blockly.FieldDropdown(nextDropDown), "NEXT_NAME");
	 },
	changeListener :	function onFirstComment(event) {
		if (event.type == Blockly.Events.CHANGE && event.name == 'NODE_NAME') {
				block = workspace.getBlockById(event.blockId);
				if(block.updateDropDown != null){
					newobject = event.newValue;
					block.updateDropDown(newobject);
				} else { 
				}
			  }
			},

  init: function() {
	var list = Object.keys(getNodeList());
	nodeDropDown = [];
	for(var i in list){
		nodeDropDown.push([list[i],list[i]]);
	}
    this.setHelpUrl(Blockly.Msg.VARIABLES_GET_HELPURL);
    this.setColour(graphCol);
	this.appendDummyInput()
		.appendField(new Blockly.FieldLabel('node', 'bold_text'));
	this.appendDummyInput('NODEDROP')
        .appendField(new Blockly.FieldDropdown(nodeDropDown), "NODE_NAME");	
	this.appendDummyInput()
		.appendField("Next")
	this.appendDummyInput('NEXTDROP')
        .appendField(new Blockly.FieldDropdown([["", "EMPTY"]]), "NEXT_NAME");
    this.setOutput(true);
    this.setTooltip("Choose a variable and set the next node in the sequence");
    this.contextMenuMsg_ = Blockly.Msg.VARIABLES_GET_CREATE_SET;
    workspace.addChangeListener(this.changeListener);
  }
}; 

Blockly.Blocks['graph_node_set'] = {
	/**
   * Block for  choosing a graph node.
   * @this Blockly.Block
   */
  init: function() {
	list = Object.keys(getNodeList());
	nodeDropDown = [];
	for(var i in list){
		nodeDropDown.push([list[i],list[i]]);
	}
    this.setHelpUrl(Blockly.Msg.VARIABLES_GET_HELPURL);
    this.setColour(graphCol);
	this.appendDummyInput()
		.appendField("Node")
        .appendField(new Blockly.FieldDropdown(nodeDropDown), "NODE_NAME");	
    this.setOutput(true);
    this.setTooltip("Choose a graph node and set it to a variable");
    this.contextMenuMsg_ = Blockly.Msg.VARIABLES_GET_CREATE_SET;
    workspace.addChangeListener(this.changeListener);
  }
}; 

Blockly.Blocks['graph_edge_set'] = {
	/**
   * Block for  choosing a graph edge.
   * @this Blockly.Block
   */
  init: function() {
	edgelist = Object.keys(edges._data);
	edgeDropDown = [];
	for(var i in edgelist){
		edgeDropDown.push([edgelist[i],edgelist[i]]);
	}
    this.setHelpUrl(Blockly.Msg.VARIABLES_GET_HELPURL);
    this.setColour(graphCol);
	this.appendDummyInput()
		.appendField("Edge")
        .appendField(new Blockly.FieldDropdown(edgeDropDown), "EDGE_NAME");	
    this.setOutput(true);
    this.setTooltip("Choose a graph edge and set it to a variable");
    this.contextMenuMsg_ = Blockly.Msg.VARIABLES_GET_CREATE_SET;
    workspace.addChangeListener(this.changeListener);
  }
}; 

Blockly.Blocks['graph_node_list'] = {
	/**
   * Block for making a list of all nodes connected to a node.
   * @this Blockly.Block
   */
  init: function() {
	list = Object.keys(getNodeList());
	nodeDropDown = [];
	for(var i in list){
		nodeDropDown.push([list[i],list[i]]);
	}
    this.setHelpUrl(Blockly.Msg.VARIABLES_GET_HELPURL);
    this.setColour(graphCol);
	this.appendDummyInput()
		.appendField("List all adjecent nodes to:")
        .appendField(new Blockly.FieldDropdown(nodeDropDown), "NODE_NAME");	
    this.setOutput(true);
    this.setTooltip("Choose a graph node and list the nodes adjecent to it");
    this.contextMenuMsg_ = Blockly.Msg.VARIABLES_GET_CREATE_SET;
    workspace.addChangeListener(this.changeListener);
  }
}; 

Blockly.Blocks['graph_node_highlight'] = {
	/**
   * Block for highlighting a node.
   * @this Blockly.Block
   */
  init: function() {
	list = Object.keys(getNodeList());
	nodeDropDown = [];
	for(var i in list){
		nodeDropDown.push([list[i],list[i]]);
	}
    this.setHelpUrl(Blockly.Msg.VARIABLES_GET_HELPURL);
    this.setColour(graphCol);
	this.appendDummyInput()
		.appendField("Highlight node:")
        .appendField(new Blockly.FieldDropdown(nodeDropDown), "NODE_NAME");	
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip("Choose a graph node and highlight it");
    this.contextMenuMsg_ = Blockly.Msg.VARIABLES_GET_CREATE_SET;
    workspace.addChangeListener(this.changeListener);
  }
}; 

Blockly.Blocks['graph_edge_highlight'] = {
	/**
   * Block for highlighting an edge.
   * @this Blockly.Block
   */
  init: function() {
	list = Object.keys(edges._data);
	nodeDropDown = [];
	for(var i in list){
		nodeDropDown.push([list[i],list[i]]);
	}
    this.setHelpUrl(Blockly.Msg.VARIABLES_GET_HELPURL);
    this.setColour(graphCol);
	this.appendDummyInput()
		.appendField("Highlight edge:")
        .appendField(new Blockly.FieldDropdown(nodeDropDown), "EDGE_NAME");	
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip("Choose a graph edge and highlight it");
    this.contextMenuMsg_ = Blockly.Msg.VARIABLES_GET_CREATE_SET;
    workspace.addChangeListener(this.changeListener);
  }
}; 

Blockly.Blocks['graph_list'] = {
	/**
   * Block for returning a list of all nodes.
   * @this Blockly.Block
   */
  init: function() {
    this.setHelpUrl(Blockly.Msg.VARIABLES_GET_HELPURL);
    this.setColour(graphCol);
	this.appendDummyInput()
		.appendField(new Blockly.FieldLabel('node', 'bold_text'))
		.appendField("list")
    this.setOutput(true);
    this.setTooltip("Get a list of nodes.");
    this.contextMenuMsg_ = Blockly.Msg.VARIABLES_GET_CREATE_SET;
    workspace.addChangeListener(this.changeListener);
  }
}; 

Blockly.Blocks['graph_edge_list'] = {
	/**
   * Block for returning a list of all edges.
   * @this Blockly.Block
   */
  init: function() {
    this.setHelpUrl(Blockly.Msg.VARIABLES_GET_HELPURL);
    this.setColour(graphCol);
	this.appendDummyInput()
		.appendField(new Blockly.FieldLabel('edge', 'bold_text'))
		.appendField("list")
    this.setOutput(true);
    this.setTooltip("Get a list of edges.");
    this.contextMenuMsg_ = Blockly.Msg.VARIABLES_GET_CREATE_SET;
    workspace.addChangeListener(this.changeListener);
  }
}; 

Blockly.Blocks['graph_node_degree'] = {
	/**
   * Block for getting a degree of a chosen node.
   * @this Blockly.Block
   */
  init: function() {
	list = Object.keys(getNodeList());
	nodeDropDown = [];
	for(var i in list){
		nodeDropDown.push([list[i],list[i]]);
	}
    this.setHelpUrl(Blockly.Msg.VARIABLES_GET_HELPURL);
    this.setColour(graphCol);
	this.appendValueInput("NODE_NAME")
        .setCheck(null)
        .setAlign(Blockly.ALIGN_CENTRE);	
	this.appendDummyInput()
		.appendField(new Blockly.FieldLabel('.', 'super_text'))
		.appendField("degree");
    this.setOutput(true);
    this.setTooltip("Get the number of adjecent nodes to a node.");
    this.contextMenuMsg_ = Blockly.Msg.VARIABLES_GET_CREATE_SET;
    workspace.addChangeListener(this.changeListener);
  }
}; 

Blockly.Blocks['graph_add_node'] = {
	/**
   * Block for ceating a new block.
   * @this Blockly.Block
   */
  init: function() {
    this.setHelpUrl(Blockly.Msg.VARIABLES_GET_HELPURL);
    this.setColour(graphCol);
	this.appendDummyInput()
		.appendField('add')
		.appendField(new Blockly.FieldLabel('node', 'bold_text'));
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip("Add a node.");
    this.contextMenuMsg_ = Blockly.Msg.VARIABLES_GET_CREATE_SET;
    workspace.addChangeListener(this.changeListener);
  }
}; 

Blockly.Blocks['graph_remove_node'] = {
	/**
   * Block for removing a graph node.
   * @this Blockly.Block
   */
  init: function() {
	list = Object.keys(nodes._data);
    this.setHelpUrl(Blockly.Msg.VARIABLES_GET_HELPURL);
    this.setColour(graphCol);
	this.appendDummyInput()
		.appendField("Remove node:")
        .appendField(new Blockly.FieldDropdown(list), "NODE_NAME");	
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip("Choose a graph node and remove it");
    this.contextMenuMsg_ = Blockly.Msg.VARIABLES_GET_CREATE_SET;
  }
}; 

Blockly.Blocks['graph_add_edge'] = {
	/**
   * Block for adding an edge between two nodes.
   * @this Blockly.Block
   */
  init: function() {
	list = Object.keys(nodes._data);
    this.setHelpUrl(Blockly.Msg.VARIABLES_GET_HELPURL);
    this.setColour(graphCol);
	this.appendDummyInput()
		.appendField("Add edge from node:")
        .appendField(new Blockly.FieldDropdown(list), "FROM_NAME")
		.appendField("to node:")
        .appendField(new Blockly.FieldDropdown(list), "TO_NAME");	
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip("Create a new edge between two nodes");
    this.contextMenuMsg_ = Blockly.Msg.VARIABLES_GET_CREATE_SET;
  }
}; 

Blockly.Blocks['graph_remove_edge'] = {
	/**
   * Block for removing a graph edge.
   * @this Blockly.Block
   */
  init: function() {
	list = Object.keys(edges._data);
    this.setHelpUrl(Blockly.Msg.VARIABLES_GET_HELPURL);
    this.setColour(graphCol);
	this.appendDummyInput()
		.appendField("Remove edge:")
        .appendField(new Blockly.FieldDropdown(list), "EDGE_NAME");	
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip("Choose a graph edge and remove it");
    this.contextMenuMsg_ = Blockly.Msg.VARIABLES_GET_CREATE_SET;
  }
}; 

Blockly.Blocks['graph_iterate'] = {
	/**
   * Block for iterating through nodes and edges
   * @this Blockly.Block
   */
  init: function() {
	list = Object.keys(nodes._data);
    this.setHelpUrl(Blockly.Msg.VARIABLES_GET_HELPURL);
    this.setColour(graphCol);
	this.appendDummyInput()
		.appendField("Iterate from node:")
        .appendField(new Blockly.FieldDropdown(list), "NODE_NAME");	
    this.setOutput(true);
    this.setTooltip("Iterate from a node");
    this.contextMenuMsg_ = Blockly.Msg.VARIABLES_GET_CREATE_SET;
  }
}; 

Blockly.Blocks['graph_current_highlighted'] = {
	/**
   * Block for representing the currently highlighted block.
   * @this Blockly.Block
   */
  init: function() {
    this.setHelpUrl(Blockly.Msg.VARIABLES_GET_HELPURL);
    this.setColour(graphCol);
	this.appendDummyInput()
		.appendField("Current highlighted")
    this.setOutput(true);
    this.setTooltip("Get the currently highlighted node/edge.");
    this.contextMenuMsg_ = Blockly.Msg.VARIABLES_GET_CREATE_SET;
  }
}; 

Blockly.Blocks['graph_colour_node'] = {
	/**
   * Block for adding colour to a node.
   * @this Blockly.Block
   */
  init: function() {
	list = Object.keys(nodes._data);
    this.setHelpUrl(Blockly.Msg.VARIABLES_GET_HELPURL);
    this.setColour(graphCol);
	this.appendDummyInput()
		.appendField("Colour node:")
        .appendField(new Blockly.FieldDropdown(list), "NODE_NAME")
		.appendField("to:")
        .appendField(new Blockly.FieldColour('#ff0000'),"COLOUR_NAME");	
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip("Colour a node");
    this.contextMenuMsg_ = Blockly.Msg.VARIABLES_GET_CREATE_SET;
  }
}; 

Blockly.Blocks['graph_colour_edge'] = {
	/**
   * Block for adding colour to an edge.
   * @this Blockly.Block
   */
  init: function() {
	list = Object.keys(edges._data);
    this.setHelpUrl(Blockly.Msg.VARIABLES_GET_HELPURL);
    this.setColour(graphCol);
	this.appendDummyInput()
		.appendField("Colour edge:")
        .appendField(new Blockly.FieldDropdown(list), "EDGE_NAME")
		.appendField("to:")
        .appendField(new Blockly.FieldColour('#ff0000'), "COLOUR_NAME");	
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip("Colour an edge");
    this.contextMenuMsg_ = Blockly.Msg.VARIABLES_GET_CREATE_SET;
  }
}; 

Blockly.Blocks['graph_colour_var'] = {
	/**
   * Block for adding colour the graph part of a variable.
   * @this Blockly.Block
   */
  init: function() {
	list = Object.keys(edges._data);
    this.setHelpUrl(Blockly.Msg.VARIABLES_GET_HELPURL);
    this.setColour(graphCol);
	this.appendDummyInput()
	this.appendValueInput("VARIABLE")
        .setCheck(null)
        .setAlign(Blockly.ALIGN_CENTRE);
	this.appendDummyInput()
		.appendField(new Blockly.FieldLabel('.', 'super_text'))
		.appendField("colour = ")
        .appendField(new Blockly.FieldColour('#ff0000'), "COLOUR_NAME");	
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip("Colour an edge or node using a variable");
    this.contextMenuMsg_ = Blockly.Msg.VARIABLES_GET_CREATE_SET;
  }
}; 

Blockly.Blocks['node_set_value'] = {
	/**
   * Block for setting a value to a node.
   * @this Blockly.Block
   */
  init: function() {
	list = Object.keys(nodes._data);
    this.setHelpUrl(Blockly.Msg.VARIABLES_GET_HELPURL);
    this.setColour(graphCol);
	this.appendDummyInput()
		.appendField("Set value")
    this.appendValueInput("FIELD")
        .setCheck(null)
        .setAlign(Blockly.ALIGN_CENTRE);	
	this.appendDummyInput()
		.appendField("Of node")
        .appendField(new Blockly.FieldDropdown(list), "NODE_NAME")
		.appendField("to:")
    this.appendValueInput("VALUE")
        .setCheck(null)
        .setAlign(Blockly.ALIGN_CENTRE);
	this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip("Set a variable to a node");
    this.contextMenuMsg_ = Blockly.Msg.VARIABLES_GET_CREATE_SET;
  }
}; 

Blockly.Blocks['edge_set_value'] = {
	/**
   * Block for setting a value to a edge.
   * @this Blockly.Block
   */
  init: function() {
	list = Object.keys(edges._data);
	edgeDropDown = [];
	for(var i in list){
		edgeDropDown.push([list[i],list[i]]);
	}
    this.setHelpUrl(Blockly.Msg.VARIABLES_GET_HELPURL);
    this.setColour(graphCol);
	this.appendDummyInput()
		.appendField("Set value")
    this.appendValueInput("FIELD")
        .setCheck(null)
        .setAlign(Blockly.ALIGN_CENTRE);	
	this.appendDummyInput()
		.appendField("Of edge")
        .appendField(new Blockly.FieldDropdown(edgeDropDown), "EDGE_NAME")
		.appendField("to:")
    this.appendValueInput("VALUE")
        .setCheck(null)
        .setAlign(Blockly.ALIGN_CENTRE);
	this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip("Set a value to an edge.");
    this.contextMenuMsg_ = Blockly.Msg.VARIABLES_GET_CREATE_SET;
  }
}; 

Blockly.Blocks['node_get_value'] = {
	/**
   * Block for getting a value from a node.
   * @this Blockly.Block
   */
  init: function() {
	list = Object.keys(nodes._data);
    this.setHelpUrl(Blockly.Msg.VARIABLES_GET_HELPURL);
    this.setColour(graphCol);
	this.appendDummyInput()
		.appendField("Get value")
    this.appendValueInput("FIELD")
        .setCheck(null)
        .setAlign(Blockly.ALIGN_CENTRE);	
	this.appendDummyInput()
		.appendField("from node")
        .appendField(new Blockly.FieldDropdown(list), "NODE_NAME")
	this.setInputsInline(true);
    this.setOutput(true);
    this.setTooltip("Get a value from a graph node");
    this.contextMenuMsg_ = Blockly.Msg.VARIABLES_GET_CREATE_SET;
  }
}; 

Blockly.Blocks['graph_node_plug'] = {
	/**
   * Block for referencing a node with a plug.
   * @this Blockly.Block
   */
  init: function() {
    this.setHelpUrl(Blockly.Msg.VARIABLES_GET_HELPURL);
    this.setColour(graphCol);
	this.appendDummyInput()
		.appendField(new Blockly.FieldLabel('node', 'bold_text'))
    this.appendValueInput("ID")
        .setCheck('Number')
        .setAlign(Blockly.ALIGN_CENTRE);	
	this.setInputsInline(true);
    this.setOutput(true);
    this.setTooltip("Reference a node with a number");
    this.contextMenuMsg_ = Blockly.Msg.VARIABLES_GET_CREATE_SET;
  }
}; 

Blockly.Blocks['graph_edge_plug'] = {
	/**
   * Block for referencing an edge with a plug.
   * @this Blockly.Block
   */
  init: function() {

    this.setHelpUrl(Blockly.Msg.VARIABLES_GET_HELPURL);
    this.setColour(graphCol);
	this.appendDummyInput()
		.appendField(new Blockly.FieldLabel('edge', 'bold_text'))
    this.appendValueInput("ID")
        .setCheck('Number')
        .setAlign(Blockly.ALIGN_CENTRE);	
	this.setInputsInline(true);
    this.setOutput(true);
    this.setTooltip("Reference an edge with a number");
    this.contextMenuMsg_ = Blockly.Msg.VARIABLES_GET_CREATE_SET;
  }
}; 

Blockly.Blocks['edge_get_value'] = {
	/**
   * Block for getting a value from an edge.
   * @this Blockly.Block
   */
  init: function() {
	list = Object.keys(edges._data);
    this.setHelpUrl(Blockly.Msg.VARIABLES_GET_HELPURL);
    this.setColour(graphCol);
	this.appendDummyInput()
		.appendField("Get value")
    this.appendValueInput("FIELD")
        .setCheck(null)
        .setAlign(Blockly.ALIGN_CENTRE);	
	this.appendDummyInput()
		.appendField("from edge")
        .appendField(new Blockly.FieldDropdown(list), "EDGE_NAME")
	this.setInputsInline(true);
    this.setOutput(true);
    this.setTooltip("Get a value from a graph edge");
    this.contextMenuMsg_ = Blockly.Msg.VARIABLES_GET_CREATE_SET;
  }
}; 

Blockly.Blocks['graph_list_iteration'] = {
	/**
   * Block for iterating though a list of nodes,edges
   * @this Blockly.Block
   */
  init: function() {

    this.setHelpUrl(Blockly.Msg.VARIABLES_GET_HELPURL);
    this.setColour(graphCol);
	this.appendDummyInput()
		.appendField("Iterate")
		.appendField(new Blockly.FieldNumber('10'), 'ITERATIONS');
	this.appendDummyInput()
		.appendField("times through list: ")
    this.appendValueInput("LIST")
        .setCheck(null);
	this.setInputsInline(true);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip("Iterate through the nodes/edges in a list.");
    this.contextMenuMsg_ = Blockly.Msg.VARIABLES_GET_CREATE_SET;
  }
}; 

Blockly.Blocks['graph_list_loop'] = {
  /**
   * Block for appllying a statement to a list of nodes/edges.
   * @this Blockly.Block
   */
  init: function() {
	list = Object.keys(edges._data);
    this.setHelpUrl(Blockly.Msg.VARIABLES_GET_HELPURL);
    this.setColour(Blockly.Blocks.loops.HUE);
	this.appendDummyInput()
		.appendField(new Blockly.FieldLabel('for', 'bold_text'))
		.appendField("each")
	this.appendValueInput("VAR")
        .setCheck(null)
        .setAlign(Blockly.ALIGN_CENTRE);	
	this.appendDummyInput()
		.appendField("in:")
	this.appendValueInput("LIST")
        .setCheck(null)
        .setAlign(Blockly.ALIGN_CENTRE);	
	this.setInputsInline(true);
    this.appendStatementInput("CODE")
        .setCheck(null);
	this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip("Apply statment to all nodes/edges in a list")
  }
};

Blockly.Blocks['graph_choose_next'] = {
  /**
   * Block for choosing the next node or edge from a list based on highest or lowest value.
   * @this Blockly.Block
   */
  init: function() {
    this.setHelpUrl(Blockly.Msg.VARIABLES_GET_HELPURL);
    this.setColour(graphCol);
	this.appendDummyInput()
		.appendField(new Blockly.FieldLabel('select', 'bold_text'))
        .appendField(new Blockly.FieldDropdown([['Highest','Highest'],['Lowest','Lowest']]), "CHOICE")
		.appendField(new Blockly.FieldTextInput('Cost'),'VALUE');
	this.appendDummyInput()
		.appendField(new Blockly.FieldLabel('from', 'bold_text'))
	this.appendValueInput("LIST")
        .setCheck(null)
        .setAlign(Blockly.ALIGN_CENTRE);	
	this.setInputsInline(true);
	this.setOutput(true);
    this.setTooltip("Apply statment to all nodes/edges in a list")
  }
};

Blockly.Blocks['graph_node_edges'] = {
	/**
   * Block for getting a list of a node's connected edges.
   * @this Blockly.Block
   */
  init: function() {
	list = Object.keys(nodes._data);
	nodeDropDown = [];
	for(var i in list){
		nodeDropDown.push([list[i],list[i]]);
	}
    this.setHelpUrl(Blockly.Msg.VARIABLES_GET_HELPURL);
    this.setColour(graphCol);
	this.appendDummyInput()
		.appendField("Get edges incident to node ")
        .appendField(new Blockly.FieldDropdown(nodeDropDown), "NODE_NAME");	
    this.setOutput(true);
    this.setTooltip("Choose a graph node and get a list of the edges connected to it");
    this.contextMenuMsg_ = Blockly.Msg.VARIABLES_GET_CREATE_SET;
    workspace.addChangeListener(this.changeListener);
  }
}; 

Blockly.Blocks['graph_plug_edges'] = {
	/**
   * Block for getting a list of a nodes connected edges.
   * @this Blockly.Block
   */
  init: function() {
	list = Object.keys(nodes._data);
	nodeDropDown = [];
	for(var i in list){
		nodeDropDown.push([list[i],list[i]]);
	}
    this.setHelpUrl(Blockly.Msg.VARIABLES_GET_HELPURL);
    this.setColour(graphCol);
 	this.appendValueInput("NODE_NAME")
		.appendField("get")
		.appendField(new Blockly.FieldLabel('edges', 'bold_text'))
		.appendField("incident to ")
        .setCheck(null)
        .setAlign(Blockly.ALIGN_CENTRE);	
    this.setOutput(true);
    this.setTooltip("Choose a graph node and get a list of the edges connected to it");
    this.contextMenuMsg_ = Blockly.Msg.VARIABLES_GET_CREATE_SET;
    workspace.addChangeListener(this.changeListener);
  }
}; 

Blockly.Blocks['graph_plug_nodes'] = {
	/**
   * Block for getting a list of a nodes adjectent nodes.
   * @this Blockly.Block
   */
  init: function() {
	list = Object.keys(nodes._data);
	nodeDropDown = [];
	for(var i in list){
		nodeDropDown.push([list[i],list[i]]);
	}
    this.setHelpUrl(Blockly.Msg.VARIABLES_GET_HELPURL);
    this.setColour(graphCol);
 	this.appendValueInput("NODE_NAME")
		.appendField("get")
		.appendField(new Blockly.FieldLabel('nodes', 'bold_text'))
		.appendField("adjacent to ")
        .setCheck(null)
        .setAlign(Blockly.ALIGN_CENTRE);	
    this.setOutput(true);
    this.setTooltip("Choose a graph node and get a list of the nodes adjacent to it");
    this.contextMenuMsg_ = Blockly.Msg.VARIABLES_GET_CREATE_SET;
    workspace.addChangeListener(this.changeListener);
  }
}; 

Blockly.Blocks['graph_isNode'] = {
	/**
   * Block that returns true if the connected block represents a node.
   * @this Blockly.Block
   */
  init: function() {
    this.setHelpUrl(Blockly.Msg.VARIABLES_GET_HELPURL);
    this.setColour(Blockly.Blocks.logic.HUE);
 	this.appendValueInput("NODE_NAME")
		.appendField("Is a node?")
        .setCheck(null)
        .setAlign(Blockly.ALIGN_CENTRE);	
    this.setOutput(true);
    this.setTooltip("Returns TRUE if the connected block is a node");
    this.contextMenuMsg_ = Blockly.Msg.VARIABLES_GET_CREATE_SET;
    workspace.addChangeListener(this.changeListener);
  }
}; 

Blockly.Blocks['graph_opposite_node'] = {
	/**
   * Block that returns the node at the other end of an edge.
   * @this Blockly.Block
   */
  init: function() {
    this.setHelpUrl(Blockly.Msg.VARIABLES_GET_HELPURL);
    this.setColour(graphCol);
 	this.appendValueInput("EDGE_NAME")
		.appendField(new Blockly.FieldLabel('node', 'bold_text'))
		.appendField("on")
		.appendField(new Blockly.FieldLabel('edge', 'bold_text'))
        .setCheck(null)
        .setAlign(Blockly.ALIGN_CENTRE);
 	this.appendValueInput("NODE_NAME")
		.appendField("incident to")
        .setCheck(null)
        .setAlign(Blockly.ALIGN_CENTRE);	
	this.setInputsInline(true);		
    this.setOutput(true);
    this.setTooltip("Returns the next node along an edge");
    this.contextMenuMsg_ = Blockly.Msg.VARIABLES_GET_CREATE_SET;
    workspace.addChangeListener(this.changeListener);
  }
}; 

Blockly.Blocks['return'] = {
  /**
   * Block for returning with value.
   * @this Blockly.Block
   */
  init: function() {
    this.setHelpUrl(Blockly.Msg.VARIABLES_GET_HELPURL);
    this.setColour(Blockly.Blocks.loops.HUE);
	this.appendValueInput("VALUE")
		.appendField(new Blockly.FieldLabel('return', 'bold_text'))
	this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip("Return from a function")
  }
};

Blockly.Blocks['graph_change_label'] = {
	/**
   * Block that changes the label of a given node.
   * @this Blockly.Block
   */
  init: function() {
    this.setHelpUrl(Blockly.Msg.VARIABLES_GET_HELPURL);
    this.setColour(graphCol);
 	this.appendValueInput("NODE_NAME")
        .setCheck(null)
        .setAlign(Blockly.ALIGN_CENTRE);
 	this.appendValueInput("LABEL_GET")
		.appendField(new Blockly.FieldLabel('.', 'super_text'))
		.appendField("label = ")
        .setCheck(null)
        .setAlign(Blockly.ALIGN_CENTRE);	
	this.setInputsInline(true);		
	this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip("Changes the label of a given node");
    this.contextMenuMsg_ = Blockly.Msg.VARIABLES_GET_CREATE_SET;
    workspace.addChangeListener(this.changeListener);
  }
}; 

Blockly.Blocks['graph_highlight_plug'] = {
	/**
   * Block that highlights a given item.
   * @this Blockly.Block
   */
  init: function() {
    this.setHelpUrl(Blockly.Msg.VARIABLES_GET_HELPURL);
    this.setColour(graphCol);
 	this.appendValueInput("NODE_NAME")
		.appendField(new Blockly.FieldLabel('highlight', 'bold_text'))
        .setCheck(null)
        .setAlign(Blockly.ALIGN_CENTRE);
	this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip("Highlights a given node or edge");
    this.contextMenuMsg_ = Blockly.Msg.VARIABLES_GET_CREATE_SET;
    workspace.addChangeListener(this.changeListener);
  }
}; 

Blockly.Blocks['graph_colour_number'] = {
	/**
   * Block that changes the colour of a node/edge based on a number.
   * @this Blockly.Block
   */
  init: function() {
    this.setHelpUrl(Blockly.Msg.VARIABLES_GET_HELPURL);
    this.setColour(graphCol);
 	this.appendValueInput("NODE_NAME")
		.appendField("change colour of ")
        .setCheck(null)
        .setAlign(Blockly.ALIGN_CENTRE);
 	this.appendValueInput("NUMBER_GET")
		.appendField("by number")
        .setCheck(null)
        .setAlign(Blockly.ALIGN_CENTRE);	
	this.setInputsInline(true);		
	this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip("Changes the colour of a node/edge based on a number 1-10");
    this.contextMenuMsg_ = Blockly.Msg.VARIABLES_GET_CREATE_SET;
    workspace.addChangeListener(this.changeListener);
  }
}; 

Blockly.Blocks['edge_add_plug'] = {
	/**
   * Block that adds an edge between two plugged in nodes.
   * @this Blockly.Block
   */
  init: function() {
    this.setHelpUrl(Blockly.Msg.VARIABLES_GET_HELPURL);
    this.setColour(graphCol);
 	this.appendValueInput("NODE_ONE")
		.appendField("add")
		.appendField(new Blockly.FieldLabel('edge', 'bold_text'))
		.appendField("between")
        .setCheck(null)
        .setAlign(Blockly.ALIGN_CENTRE);
 	this.appendValueInput("NODE_TWO")
		.appendField("and")
        .setCheck(null)
        .setAlign(Blockly.ALIGN_CENTRE);	
	this.setInputsInline(true);		
	this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip("Creates an edge between two given nodes");
    this.contextMenuMsg_ = Blockly.Msg.VARIABLES_GET_CREATE_SET;
    workspace.addChangeListener(this.changeListener);
  }
}; 

Blockly.Blocks['edge_remove_plug'] = {
	/**
   * Block that removes an edge specified by a plug.
   * @this Blockly.Block
   */
  init: function() {
    this.setHelpUrl(Blockly.Msg.VARIABLES_GET_HELPURL);
    this.setColour(graphCol);
 	this.appendValueInput("EDGE_NAME")
		.appendField("remove")
		.appendField(new Blockly.FieldLabel('edge', 'bold_text'))
        .setCheck(null)
        .setAlign(Blockly.ALIGN_CENTRE);
	this.setInputsInline(true);		
	this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip("Removes a given edge");
    this.contextMenuMsg_ = Blockly.Msg.VARIABLES_GET_CREATE_SET;
    workspace.addChangeListener(this.changeListener);
  }
};

Blockly.Blocks['node_remove_plug'] = {
	/**
   * Block that removes a node specified by a plug.
   * @this Blockly.Block
   */
  init: function() {
    this.setHelpUrl(Blockly.Msg.VARIABLES_GET_HELPURL);
    this.setColour(graphCol);
 	this.appendValueInput("NODE_NAME")
		.appendField("remove")
		.appendField(new Blockly.FieldLabel('node', 'bold_text'))
        .setCheck(null)
        .setAlign(Blockly.ALIGN_CENTRE);
	this.setInputsInline(true);		
	this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip("Removes a given node");
    this.contextMenuMsg_ = Blockly.Msg.VARIABLES_GET_CREATE_SET;
    workspace.addChangeListener(this.changeListener);
  }
};

Blockly.Blocks['graph_value_plug'] = {
	/**
   * Block for getting a value from an node/edge.
   * @this Blockly.Block
   */
  init: function() {
	list = Object.keys(edges._data);
    this.setHelpUrl(Blockly.Msg.VARIABLES_GET_HELPURL);
    this.setColour(graphCol);
	this.appendDummyInput()
		.appendField(new Blockly.FieldLabel('node', 'bold_text'))
    this.appendValueInput("ID")
        .setCheck(null)
        .setAlign(Blockly.ALIGN_CENTRE);	
	this.appendDummyInput()
		.appendField(new Blockly.FieldLabel('.', 'super_text'))
	this.appendValueInput("FIELD")
        .setCheck(null)
        .setAlign(Blockly.ALIGN_CENTRE);
	this.setInputsInline(true);
    this.setOutput(true);
    this.setTooltip("Get a value from a graph item");
    this.contextMenuMsg_ = Blockly.Msg.VARIABLES_GET_CREATE_SET;
  }
};
Blockly.Blocks['graph_value_plug_edge'] = {
	/**
   * Block for getting a value from an node/edge.
   * @this Blockly.Block
   */
  init: function() {
	list = Object.keys(edges._data);
    this.setHelpUrl(Blockly.Msg.VARIABLES_GET_HELPURL);
    this.setColour(graphCol);
	this.appendDummyInput()
		.appendField(new Blockly.FieldLabel('edge', 'bold_text'))
    this.appendValueInput("ID")
        .setCheck(null)
        .setAlign(Blockly.ALIGN_CENTRE);	
	this.appendDummyInput()
		.appendField(new Blockly.FieldLabel('.', 'super_text'))
	this.appendValueInput("FIELD")
        .setCheck(null)
        .setAlign(Blockly.ALIGN_CENTRE);
	this.setInputsInline(true);
    this.setOutput(true);
    this.setTooltip("Get a value from a graph item");
    this.contextMenuMsg_ = Blockly.Msg.VARIABLES_GET_CREATE_SET;
  }
};  

Blockly.Blocks['set_value_plug'] = {
	/**
   * Block for setting a value of a node/edge.
   * @this Blockly.Block
   */
  init: function() {
	list = Object.keys(edges._data);
    this.setHelpUrl(Blockly.Msg.VARIABLES_GET_HELPURL);
    this.setColour(graphCol);
	this.appendDummyInput()
		.appendField(new Blockly.FieldLabel('node', 'bold_text'))
    this.appendValueInput("ID")
        .setCheck(null)
        .setAlign(Blockly.ALIGN_CENTRE);
	this.appendDummyInput()
		.appendField(new Blockly.FieldLabel('.', 'super_text'))
    this.appendValueInput("FIELD")
        .setCheck(null)
        .setAlign(Blockly.ALIGN_CENTRE);			
	this.appendDummyInput()
		.appendField("=")
	this.appendValueInput("VALUE")
        .setCheck(null)
        .setAlign(Blockly.ALIGN_CENTRE);
	this.setInputsInline(true);
	this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip("Set a value of a graph node");
    this.contextMenuMsg_ = Blockly.Msg.VARIABLES_GET_CREATE_SET;
  }
};
Blockly.Blocks['set_value_plug_edge'] = {
	/**
   * Block for setting a value of a node/edge.
   * @this Blockly.Block
   */
  init: function() {
	list = Object.keys(edges._data);
    this.setHelpUrl(Blockly.Msg.VARIABLES_GET_HELPURL);
    this.setColour(graphCol);
	this.appendDummyInput()
		.appendField(new Blockly.FieldLabel('edge', 'bold_text'))
    this.appendValueInput("ID")
        .setCheck(null)
        .setAlign(Blockly.ALIGN_CENTRE);
	this.appendDummyInput()
		.appendField(new Blockly.FieldLabel('.', 'super_text'))
    this.appendValueInput("FIELD")
        .setCheck(null)
        .setAlign(Blockly.ALIGN_CENTRE);			
	this.appendDummyInput()
		.appendField("=")
	this.appendValueInput("VALUE")
        .setCheck(null)
        .setAlign(Blockly.ALIGN_CENTRE);
	this.setInputsInline(true);
	this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip("Set a value of an edge");
    this.contextMenuMsg_ = Blockly.Msg.VARIABLES_GET_CREATE_SET;
  }
}; 