/**
 * @license
 * Visual Blocks Editor
 *
 * Copyright 2012 Google Inc.
 * https://developers.google.com/blockly/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @fileoverview List blocks for Blockly.
 * @author fraser@google.com (Neil Fraser)
 */
'use strict';

goog.provide('Blockly.Blocks.lists');

goog.require('Blockly.Blocks');


/**
 * Common HSV hue for all blocks in this category.
 */
Blockly.Blocks.lists.HUE = 260;

Blockly.Blocks['lists_create_empty'] = {
  /**
   * Block for creating an empty list.
   * The 'list_create_with' block is preferred as it is more flexible.
   * <block type="lists_create_with">
   *   <mutation items="0"></mutation>
   * </block>
   * @this Blockly.Block
   */
  init: function() {
    this.jsonInit({
      "message0": "%1",
	  "args0":[
	  {
		 "type": "field_label",
		 "text": "[ ]",
		 "class": "bold_text" 
	  }
	  ],
      "output": "Array",
      "colour": listCol,
      "tooltip": Blockly.Msg.LISTS_CREATE_EMPTY_TOOLTIP,
      "helpUrl": Blockly.Msg.LISTS_CREATE_EMPTY_HELPURL
    });
  }
};

Blockly.Blocks['lists_create_with'] = {
  /**
   * Block for creating a list with any number of elements of any type.
   * @this Blockly.Block
   */
  init: function() {
	this.appendDummyInput("OPENSQ").appendField(new Blockly.FieldLabel('[', 'brack_text'));
	this.appendDummyInput("CLOSESQ").appendField(new Blockly.FieldLabel(']', 'brack_text'));
    this.setHelpUrl(Blockly.Msg.LISTS_CREATE_WITH_HELPURL);
    this.setColour(listCol);
    this.itemCount_ = 3;
    this.updateShape_();
    this.setOutput(true, 'Array');
	this.setInputsInline(true);
    this.setMutator(new Blockly.Mutator(['lists_create_with_item']));
    this.setTooltip(Blockly.Msg.LISTS_CREATE_WITH_TOOLTIP);
  },
  /**
   * Create XML to represent list inputs.
   * @return {!Element} XML storage element.
   * @this Blockly.Block
   */
  mutationToDom: function() {
    var container = document.createElement('mutation');
    container.setAttribute('items', this.itemCount_);
    return container;
  },
  /**
   * Parse XML to restore the list inputs.
   * @param {!Element} xmlElement XML storage element.
   * @this Blockly.Block
   */
  domToMutation: function(xmlElement) {
    this.itemCount_ = parseInt(xmlElement.getAttribute('items'), 10);
    this.updateShape_();
  },
  /**
   * Populate the mutator's dialog with this block's components.
   * @param {!Blockly.Workspace} workspace Mutator's workspace.
   * @return {!Blockly.Block} Root block in mutator.
   * @this Blockly.Block
   */
  decompose: function(workspace) {
    var containerBlock = workspace.newBlock('lists_create_with_container');
    containerBlock.initSvg();
    var connection = containerBlock.getInput('STACK').connection;
    for (var i = 0; i < this.itemCount_; i++) {
      var itemBlock = workspace.newBlock('lists_create_with_item');
      itemBlock.initSvg();
      connection.connect(itemBlock.previousConnection);
      connection = itemBlock.nextConnection;
    }
    return containerBlock;
  },
  /**
   * Reconfigure this block based on the mutator dialog's components.
   * @param {!Blockly.Block} containerBlock Root block in mutator.
   * @this Blockly.Block
   */
  compose: function(containerBlock) {
    var itemBlock = containerBlock.getInputTargetBlock('STACK');
    // Count number of inputs.
    var connections = [];
    while (itemBlock) {
      connections.push(itemBlock.valueConnection_);
      itemBlock = itemBlock.nextConnection &&
          itemBlock.nextConnection.targetBlock();
    }
    // Disconnect any children that don't belong.
    for (var i = 0; i < this.itemCount_; i++) {
      var connection = this.getInput('ADD' + i).connection.targetConnection;
      if (connection && connections.indexOf(connection) == -1) {
        connection.disconnect();
      }
    }
    this.itemCount_ = connections.length;
    this.updateShape_();
    // Reconnect any child blocks.
    for (var i = 0; i < this.itemCount_; i++) {
      Blockly.Mutator.reconnect(connections[i], this, 'ADD' + i);
    }
  },
  /**
   * Store pointers to any connected child blocks.
   * @param {!Blockly.Block} containerBlock Root block in mutator.
   * @this Blockly.Block
   */
  saveConnections: function(containerBlock) {
    var itemBlock = containerBlock.getInputTargetBlock('STACK');
    var i = 0;
    while (itemBlock) {
      var input = this.getInput('ADD' + i);
      itemBlock.valueConnection_ = input && input.connection.targetConnection;
      i++;
      itemBlock = itemBlock.nextConnection &&
          itemBlock.nextConnection.targetBlock();
    }
  },
  /**
   * Modify this block to have the correct number of inputs.
   * @private
   * @this Blockly.Block
   */
  updateShape_: function() {
	this.removeInput("CLOSESQ");
    if (this.itemCount_ && this.getInput('EMPTY')) {
      this.removeInput('EMPTY');
    } else if (!this.itemCount_ && !this.getInput('EMPTY')) {
      this.appendDummyInput('EMPTY')
          .appendField(Blockly.Msg.LISTS_CREATE_EMPTY_TITLE);
    }
    // Add new inputs.
    for (var i = 0; i < this.itemCount_; i++) {
      if (!this.getInput('ADD' + i)) {
		 if(i > 0){
			this.appendDummyInput().appendField(", ");
		 }
        var input = this.appendValueInput('ADD' + i);
		
        if (i == 0) {
          input.appendField(Blockly.Msg.LISTS_CREATE_WITH_INPUT_WITH);
        }
      }
    }
	this.appendDummyInput("CLOSESQ").appendField(new Blockly.FieldLabel(']', 'brack_text'));
    // Remove deleted inputs.
    while (this.getInput('ADD' + i)) {
      this.removeInput('ADD' + i);
      i++;
    }
  }
};

Blockly.Blocks['lists_create_with_container'] = {
  /**
   * Mutator block for list container.
   * @this Blockly.Block
   */
  init: function() {
    this.setColour(listCol);
    this.appendDummyInput()
        .appendField(Blockly.Msg.LISTS_CREATE_WITH_CONTAINER_TITLE_ADD);
    this.appendStatementInput('STACK');
    this.setTooltip(Blockly.Msg.LISTS_CREATE_WITH_CONTAINER_TOOLTIP);
    this.contextMenu = false;
  }
};

Blockly.Blocks['lists_create_with_item'] = {
  /**
   * Mutator bolck for adding items.
   * @this Blockly.Block
   */
  init: function() {
    this.setColour(listCol);
    this.appendDummyInput()
        .appendField(Blockly.Msg.LISTS_CREATE_WITH_ITEM_TITLE);
    this.setPreviousStatement(true);
    this.setNextStatement(true);
    this.setTooltip(Blockly.Msg.LISTS_CREATE_WITH_ITEM_TOOLTIP);
    this.contextMenu = false;
  }
};

Blockly.Blocks['lists_repeat'] = {
  /**
   * Block for creating a list with one element repeated.
   * @this Blockly.Block
   */
  init: function() {
    this.jsonInit({
      "message0": Blockly.Msg.LISTS_REPEAT_TITLE,
      "args0": [
        {
          "type": "input_value",
          "name": "ITEM"
        },
        {
          "type": "input_value",
          "name": "NUM",
          "check": "Number"
        }
      ],
      "output": "Array",
      "colour": listCol,
      "tooltip": Blockly.Msg.LISTS_REPEAT_TOOLTIP,
      "helpUrl": Blockly.Msg.LISTS_REPEAT_HELPURL
    });
  }
};

Blockly.Blocks['lists_length'] = {
  /**
   * Block for list length.
   * @this Blockly.Block
   */
  init: function() {
    this.jsonInit({
      "message0": "%1%2length",
      "args0": [
        {
          "type": "input_value",
          "name": "VALUE",
          "check": ['String', 'Array']
        },
		{
			"type": "field_label",
			"text": ".",
			"class": "super_text"
		}
      ],
      "output": 'Number',
      "colour": listCol,
      "tooltip": Blockly.Msg.LISTS_LENGTH_TOOLTIP,
      "helpUrl": Blockly.Msg.LISTS_LENGTH_HELPURL
    });
  }
};

Blockly.Blocks['lists_isEmpty'] = {
  /**
   * Block for is the list empty?
   * @this Blockly.Block
   */
  init: function() {
    this.jsonInit({
      "message0": Blockly.Msg.LISTS_ISEMPTY_TITLE,
      "args0": [
        {
          "type": "input_value",
          "name": "VALUE",
          "check": ['String', 'Array']
        }
      ],
      "output": 'Boolean',
      "colour": listCol,
      "tooltip": Blockly.Msg.LISTS_ISEMPTY_TOOLTIP,
      "helpUrl": Blockly.Msg.LISTS_ISEMPTY_HELPURL
    });
  }
};

Blockly.Blocks['lists_indexOf'] = {
  /**
   * Block for finding an item in the list.
   * @this Blockly.Block
   */
  init: function() {
    var OPERATORS =
        [[Blockly.Msg.LISTS_INDEX_OF_FIRST, 'FIRST'],
         [Blockly.Msg.LISTS_INDEX_OF_LAST, 'LAST']];
    this.setHelpUrl(Blockly.Msg.LISTS_INDEX_OF_HELPURL);
    this.setColour(listCol);
    this.setOutput(true, 'Number');
    this.appendValueInput('VALUE')
        .setCheck('Array')
        .appendField(Blockly.Msg.LISTS_INDEX_OF_INPUT_IN_LIST);
    this.appendValueInput('FIND')
        .appendField(new Blockly.FieldDropdown(OPERATORS), 'END');
    this.setInputsInline(true);
    this.setTooltip(Blockly.Msg.LISTS_INDEX_OF_TOOLTIP);
  }
};

Blockly.Blocks['lists_getIndex'] = {
  /**
   * Block for getting element at index.
   * @this Blockly.Block
   */
  init: function() {
      this.jsonInit({
   "message0": '%1 %2 %3 %4',
      "args0": [
	    {
          "type": "field_variable",
          "name": "VAR",
          "variable": null
        }, 
		{
			"type": "field_label",
			"text": "[",
			"class": "brack_text"
		},
        {
          "type": "input_value",
          "name": "NUM",
          "check": "Number",
          "align": "RIGHT"
        },
		{
		"type": "field_label",
		"text": "]",
		"class": "brack_text"
		}
      ],
      "inputsInline": true,
      "output": true,
      "colour": listCol,
      "helpUrl": Blockly.Msg.CONTROLS_FOR_HELPURL
    });
  }
};

Blockly.Blocks['lists_killIndex'] = {
  /**
   * Block for getting removing an element at index.
   * @this Blockly.Block
   */
  init: function() {
      this.jsonInit({
   "message0": '%1 %2 %3 %4 %5',
      "args0": [
	  	{
			"type": "field_label",
			"text": "remove",
		},
	    {
          "type": "field_variable",
          "name": "VAR",
          "variable": null
        }, 
		{
			"type": "field_label",
			"text": "[",
			"class": "brack_text"
		},
        {
          "type": "input_value",
          "name": "NUM",
          "check": "Number",
          "align": "RIGHT"
        },
		{
		"type": "field_label",
		"text": "]",
		"class": "brack_text"
		}
      ],
      "inputsInline": true,
      "previousStatement": null,
      "nextStatement": null,
      "colour": listCol,
      "helpUrl": Blockly.Msg.CONTROLS_FOR_HELPURL
    });
  }
};

Blockly.Blocks['lists_setIndex'] = {
  /**
   * Block for setting the element at index.
   * @this Blockly.Block
   */
  init: function() {
	this.setHelpUrl(Blockly.Msg.VARIABLES_GET_HELPURL);
    this.setColour(listCol);
    this.appendDummyInput()
		.appendField(new Blockly.FieldVariable(
		Blockly.Msg.VARIABLES_DEFAULT_NAME), 'VAR');
	this.appendValueInput("NUM")
		.appendField(new Blockly.FieldLabel('[', 'brack_text'));
	this.appendValueInput("INPUT")
		.appendField(new Blockly.FieldLabel('] =', 'brack_text'))
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
	this.setInputsInline(true);
    this.setTooltip(Blockly.Msg.VARIABLES_GET_TOOLTIP);
    this.contextMenuMsg_ = Blockly.Msg.VARIABLES_GET_CREATE_SET;
  }
};

Blockly.Blocks['lists_getSublist'] = {
  /**
   * Block for getting sublist.
   * @this Blockly.Block
   */
  init: function() {
	  this.jsonInit({
   "message0": '%1 %2 %3 %4 %5 %6',
      "args0": [
	    {
          "type": "field_variable",
          "name": "VAR",
          "variable": null
        }, 
		{
		"type": "field_label",
      "text": "[",
      "class": "brack_text"
	  },
        {
          "type": "input_value",
          "name": "FROM",
          "check": "Number",
          "align": "RIGHT"
        },
		{
		"type": "field_label",
		"text": ":",
		"class": "bold_text"
	  },
        {
          "type": "input_value",
          "name": "TO",
          "check": "Number",
          "align": "RIGHT"
        },
		{
		"type": "field_label",
		"text": "]",
		"class": "brack_text"
		}
      ],
      "inputsInline": true,
      "output": true,
      "colour": listCol,
      "helpUrl": Blockly.Msg.CONTROLS_FOR_HELPURL
    });
  }
};



Blockly.Blocks['lists_sort'] = {
  /**
   * Block for sorting a list.
   * @this Blockly.Block
   */
  init: function() {
    this.jsonInit({
      "message0": Blockly.Msg.LISTS_SORT_TITLE,
      "args0": [
        {
          "type": "field_dropdown",
          "name": "TYPE",
          "options": [
            [Blockly.Msg.LISTS_SORT_TYPE_NUMERIC, "NUMERIC"],
            [Blockly.Msg.LISTS_SORT_TYPE_TEXT, "TEXT"],
            [Blockly.Msg.LISTS_SORT_TYPE_IGNORECASE, "IGNORE_CASE"]
          ]
        },
        {
          "type": "field_dropdown",
          "name": "DIRECTION",
          "options": [
            [Blockly.Msg.LISTS_SORT_ORDER_ASCENDING, "1"],
            [Blockly.Msg.LISTS_SORT_ORDER_DESCENDING, "-1"]
          ]
        },
        {
          "type": "input_value",
          "name": "LIST",
          "check": "Array"
        }
      ],
      "output": "Array",
      "colour": listCol,
      "tooltip": Blockly.Msg.LISTS_SORT_TOOLTIP,
      "helpUrl": Blockly.Msg.LISTS_SORT_HELPURL
    });
  }
};

Blockly.Blocks['lists_split'] = {
  /**
   * Block for splitting text into a list, or joining a list into text.
   * @this Blockly.Block
   */
  init: function() {
    // Assign 'this' to a variable for use in the closures below.
    var thisBlock = this;
    var dropdown = new Blockly.FieldDropdown(
        [[Blockly.Msg.LISTS_SPLIT_LIST_FROM_TEXT, 'SPLIT'],
         [Blockly.Msg.LISTS_SPLIT_TEXT_FROM_LIST, 'JOIN']],
        function(newMode) {
          thisBlock.updateType_(newMode);
        });
    this.setHelpUrl(Blockly.Msg.LISTS_SPLIT_HELPURL);
    this.setColour(listCol);
    this.appendValueInput('INPUT')
        .setCheck('String')
        .appendField(dropdown, 'MODE');
    this.appendValueInput('DELIM')
        .setCheck('String')
        .appendField(Blockly.Msg.LISTS_SPLIT_WITH_DELIMITER);
    this.setInputsInline(true);
    this.setOutput(true, 'Array');
    this.setTooltip(function() {
      var mode = thisBlock.getFieldValue('MODE');
      if (mode == 'SPLIT') {
        return Blockly.Msg.LISTS_SPLIT_TOOLTIP_SPLIT;
      } else if (mode == 'JOIN') {
        return Blockly.Msg.LISTS_SPLIT_TOOLTIP_JOIN;
      }
      throw 'Unknown mode: ' + mode;
    });
  },
  /**
   * Modify this block to have the correct input and output types.
   * @param {string} newMode Either 'SPLIT' or 'JOIN'.
   * @private
   * @this Blockly.Block
   */
  updateType_: function(newMode) {
    if (newMode == 'SPLIT') {
      this.outputConnection.setCheck('Array');
      this.getInput('INPUT').setCheck('String');
    } else {
      this.outputConnection.setCheck('String');
      this.getInput('INPUT').setCheck('Array');
    }
  },
  /**
   * Create XML to represent the input and output types.
   * @return {!Element} XML storage element.
   * @this Blockly.Block
   */
  mutationToDom: function() {
    var container = document.createElement('mutation');
    container.setAttribute('mode', this.getFieldValue('MODE'));
    return container;
  },
  /**
   * Parse XML to restore the input and output types.
   * @param {!Element} xmlElement XML storage element.
   * @this Blockly.Block
   */
  domToMutation: function(xmlElement) {
    this.updateType_(xmlElement.getAttribute('mode'));
  }
};


Blockly.Blocks['lists_add_to'] = {
	/**
   * Block that adds a value to a list.
   * @this Blockly.Block
   */
  init: function() {
    this.setHelpUrl(Blockly.Msg.VARIABLES_GET_HELPURL);
    this.setColour(listCol);
 	this.appendValueInput("ITEM_NAME")
		.appendField("Add")
        .setCheck(null)
        .setAlign(Blockly.ALIGN_CENTRE);
 	this.appendValueInput("LIST_NAME")
		.appendField("to list")
        .setCheck(null)
        .setAlign(Blockly.ALIGN_CENTRE);	
	this.setInputsInline(true);		
    this.setOutput(true);
    this.setTooltip("Adds a given item to a list.");
    this.contextMenuMsg_ = Blockly.Msg.VARIABLES_GET_CREATE_SET;
  }
}; 

Blockly.Blocks['lists_is_in'] = {
	/**
   * Block that returns true if an item is in a list.
   * @this Blockly.Block
   */
  init: function() {
    this.setHelpUrl(Blockly.Msg.VARIABLES_GET_HELPURL);
    this.setColour(listCol);
 	this.appendValueInput("ITEM_NAME")
        .setCheck(null)
        .setAlign(Blockly.ALIGN_CENTRE);
 	this.appendValueInput("LIST_NAME")
		.appendField(new Blockly.FieldLabel('in', 'bold_text'))
        .setCheck(null)
        .setAlign(Blockly.ALIGN_CENTRE);	
	this.setInputsInline(true);		
    this.setOutput(true);
    this.setTooltip("Returns true if an item is in a list.");
    this.contextMenuMsg_ = Blockly.Msg.VARIABLES_GET_CREATE_SET;
  }
};

Blockly.Blocks['lists_merge'] = {
	/**
   * Block that merges two lists together.
   * @this Blockly.Block
   */
  init: function() {
    this.setHelpUrl(Blockly.Msg.VARIABLES_GET_HELPURL);
    this.setColour(listCol);
 	this.appendValueInput("LISTONE")
        .setCheck(null)
        .setAlign(Blockly.ALIGN_CENTRE);
 	this.appendValueInput("LISTTWO")
		.appendField(new Blockly.FieldLabel('∪	', 'brack_text'))
        .setCheck(null)
        .setAlign(Blockly.ALIGN_CENTRE);	
	this.setInputsInline(true);		
    this.setOutput(true);
    this.setTooltip("Merges two arrays together and returns the result.");
    this.contextMenuMsg_ = Blockly.Msg.VARIABLES_GET_CREATE_SET;
  }
};

Blockly.Blocks['lists_difference'] = {
	/**
   * Block that returns the difference between two lists.
   * @this Blockly.Block
   */
  init: function() {
    this.setHelpUrl(Blockly.Msg.VARIABLES_GET_HELPURL);
    this.setColour(listCol);
	this.appendDummyInput()
		.appendField(new Blockly.FieldVariable(
		Blockly.Msg.VARIABLES_DEFAULT_NAME), 'VAR');
 	this.appendValueInput("ONE_NAME")
		.appendField(new Blockly.FieldLabel('=', 'bold_text'))
        .setCheck(null)
        .setAlign(Blockly.ALIGN_CENTRE);
 	this.appendValueInput("TWO_NAME")
		.appendField(new Blockly.FieldLabel('\\', 'super_text'))
        .setCheck(null)
        .setAlign(Blockly.ALIGN_CENTRE);	
	this.setInputsInline(true);		
  	this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip("Returns the difference between two lists.");
    this.contextMenuMsg_ = Blockly.Msg.VARIABLES_GET_CREATE_SET;
  }
};

Blockly.Blocks['lists_difference2'] = {
	/**
   * Block that returns the difference between two lists.
   * @this Blockly.Block
   */
  init: function() {
    this.setHelpUrl(Blockly.Msg.VARIABLES_GET_HELPURL);
    this.setColour(listCol);
 	this.appendValueInput("ONE_NAME")
		.appendField("Items in")
        .setCheck(null)
        .setAlign(Blockly.ALIGN_CENTRE);
 	this.appendValueInput("TWO_NAME")
		.appendField("not in")
        .setCheck(null)
        .setAlign(Blockly.ALIGN_CENTRE);	
	this.setInputsInline(true);		
  	this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setTooltip("Returns the difference between two lists.");
    this.contextMenuMsg_ = Blockly.Msg.VARIABLES_GET_CREATE_SET;
  }
};

Blockly.Blocks['lists_reverse_list'] = {
	/**
   * Block for reversing the contents on a list.
   * @this Blockly.Block
   */
  init: function() {
    this.setHelpUrl(Blockly.Msg.VARIABLES_GET_HELPURL);
    this.setColour(listCol);
	this.appendDummyInput()
		.appendField(new Blockly.FieldLabel('reverse', 'bold_text'))
	this.appendValueInput("LIST")
        .setCheck(null)
        .setAlign(Blockly.ALIGN_CENTRE);
	this.setInputsInline(true);
    this.setOutput(true);
    this.setTooltip("Reverse the contents of a list.");
    this.contextMenuMsg_ = Blockly.Msg.VARIABLES_GET_CREATE_SET;
    workspace.addChangeListener(this.changeListener);
  }
}; 