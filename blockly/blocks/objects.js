/**
 * @license
 * Algorithm visualizer
 *
 * Copyright 2016 University of Leeds.
 * https://www.leeds.ac.uk/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/
 
/**
 * @fileoverview Object blocks for Blockly.
 * @author Luke Roberts
 */

 Blockly.Blocks['new_object'] = {
  /**
   * Block for creating a list with any number of elements of any type.
   * @this Blockly.Block
   */
  init: function() {
	  this.setColour(objCol);
	  this.appendDummyInput()
		.appendField(new Blockly.FieldLabel('{  }', 'bold_text'))
	  this.setOutput(true);
	  this.setTooltip('Creates a new object that can be attached to a variable.');
  }
 };
 
  Blockly.Blocks['object_value_get'] = {
  /**
   * Block for accessing the values held in an object.
   * @this Blockly.Block
   */
  init: function() {
	  this.setColour(objCol);
	  this.appendDummyInput()
		.appendField(new Blockly.FieldVariable(
		Blockly.Msg.VARIABLES_DEFAULT_NAME), 'VAR');
	  this.appendValueInput("FIELD")
		.appendField(new Blockly.FieldLabel('.', 'super_text'))
	  this.setInputsInline(true);
	  this.setOutput(true);
	  this.setTooltip('Access a value held in an object.');
  }
 };
 
  Blockly.Blocks['object_access'] = {
  /**
   * Block for changing the field in an object
   * @this Blockly.Block
   */
  init: function() {
	  this.setColour(objCol);
	  this.appendDummyInput()
		.appendField(new Blockly.FieldVariable(
		Blockly.Msg.VARIABLES_DEFAULT_NAME), 'VAR');
	  this.appendValueInput("FIELD")
		.appendField(new Blockly.FieldLabel('.', 'super_text'))
	  this.appendValueInput("VALUE")
		.appendField("=")
	  this.setInputsInline(true);
	  this.setPreviousStatement(true, null);
	  this.setNextStatement(true, null);
	  this.setTooltip('Changes the values inside an object.');
  }
 };