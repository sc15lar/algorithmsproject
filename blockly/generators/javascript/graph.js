/**
 * @license
 * Algorithm visualizer
 *
 * Copyright 2016 University of Leeds.
 * https://www.leeds.ac.uk/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/
 
/**
 * @fileoverview Generating JavaScript for graph blocks.
 * @author Luke Roberts
 */


Blockly.JavaScript['graph_node_set'] = function(block) {
	//Get the name of the given node
	var node =block.getFieldValue('NODE_NAME');
	return ['"Node '+node+'"', Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['graph_edge_set'] = function(block) {
	//Get the name of the given node
	var edge =block.getFieldValue('EDGE_NAME');
	return ['"Edge '+edge+'"', Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['graph_node_next'] = function(block) {
	//Get the name of the given node
	var node =block.getFieldValue('NEXT_NAME');
	return ['"Node '+node+'"', Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['graph_node_list'] = function(block) {
	//Get the name of the given node
	var node =block.getFieldValue('NODE_NAME');
	//Get a list of graph nodes
	graph = getNodeList();
	//Find the other nodes it is connect too
	graph = Object.keys(graph[node]);
	//Place them in an array
	adjecent = [];
	for(var i in graph){
		adjecent.push('"Node '+graph[i]+'"');
	}
	//Return this list
	return ['['+adjecent.toString()+']', Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['graph_node_highlight'] = function(block) {
	//Get the name of the given node
	var node =block.getFieldValue('NODE_NAME');
	return 'highlightNode\("'+node+'"\);\n';
};

Blockly.JavaScript['graph_edge_highlight'] = function(block) {
	//Get the name of the given edge
	var edge =block.getFieldValue('EDGE_NAME');
	return 'highlightEdge\("'+edge+'"\);\n';
};

Blockly.JavaScript['graph_list'] = function(block) {
	return ['getNodes().split(",")', Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['graph_edge_list'] = function(block) {
	//Get a lf graph nodes
	edge = edges._data;
	//Find the other nodes it is connect too
	edge = Object.keys(edge);
	//Add brackets to the keys
	var set = [];
	for(var i in edge){
		set.push("'Edge "+edge[i]+"'");
	}
	//Return this list
	return ['['+set.toString()+']', Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['graph_node_degree'] = function(block) {
	//Get the name of the given node
	var node =Blockly.JavaScript.valueToCode(block, 'NODE_NAME', Blockly.JavaScript.ORDER_ADDITION) || '0';
	return ["getDegree("+node+")", Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['graph_add_node'] = function(block) {
	return "addNode\(\);\n";
};

Blockly.JavaScript['graph_remove_node'] = function(block) {
	//Get the name of the given node
	var node =block.getFieldValue('NODE_NAME');
	return 	"removeNode\("+node+"\);\n";
};

Blockly.JavaScript['graph_add_edge'] = function(block) {
	//Get the name of the given node
	var from =block.getFieldValue('FROM_NAME');
	var to =block.getFieldValue('TO_NAME');
	return 	"addEdge\('"+from+"','"+to+"'\);\n";
};

Blockly.JavaScript['graph_remove_edge'] = function(block) {
	//Get the name of the given node
	var edge =block.getFieldValue('EDGE_NAME');
	return 	"removeEdge\("+edge+"\);\n";
};

Blockly.JavaScript['graph_iterate'] = function(block) {
	//Get the name of the given node
	var node =block.getFieldValue('NODE_NAME');
	return 	["iterate\("+node+"\)", Blockly.JavaScript.ORDER_ATOMIC]
};

Blockly.JavaScript['graph_current_highlighted'] = function(block) {
	//Check that there is a currently highlighted node/edge
	if(curHigh != undefined){
			code= curHigh.id;
	}
	else{
		code = 'undefined';
	}
	return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['graph_colour_node'] = function(block) {
	//Get the name of the given node
	var node =block.getFieldValue('NODE_NAME');
	//Get the colour
	var colour = block.getFieldValue('COLOUR_NAME');
	return 	"nodeColour\("+node+",'"+colour+"'\);\n";
};

Blockly.JavaScript['graph_colour_edge'] = function(block) {
	//Get the name of the given node
	var edge =block.getFieldValue('EDGE_NAME');
	//Get the colour
	var colour = block.getFieldValue('COLOUR_NAME');
	return 	"edgeColour\("+edge+",'"+colour+"'\);\n";
};

Blockly.JavaScript['node_set_value'] = function(block) {
	//Get the name of the given node
	var node =block.getFieldValue('NODE_NAME');
	//Get the field value
	var field = Blockly.JavaScript.valueToCode(block, 'FIELD', Blockly.JavaScript.ORDER_ADDITION) || '0';
	//Get the value
	var value = Blockly.JavaScript.valueToCode(block, 'VALUE', Blockly.JavaScript.ORDER_ADDITION) || '0';
	return 	"setNodeValue\("+node+","+field+","+value+"\);\n";
};

Blockly.JavaScript['edge_set_value'] = function(block) {
	//Get the name of the given node
	var edge =block.getFieldValue('EDGE_NAME');
	//Get the field value
	var field = Blockly.JavaScript.valueToCode(block, 'FIELD', Blockly.JavaScript.ORDER_ADDITION) || '0';
	//Get the value
	var value = Blockly.JavaScript.valueToCode(block, 'VALUE', Blockly.JavaScript.ORDER_ADDITION) || '0';
	return 	"setEdgeValue\("+edge+","+field+","+value+"\);\n";
};

Blockly.JavaScript['node_get_value'] = function(block) {
	//Get the name of the given node
	var node =block.getFieldValue('NODE_NAME');
	//Get the field value
	var field = Blockly.JavaScript.valueToCode(block, 'FIELD', Blockly.JavaScript.ORDER_ADDITION) || '0';
	return 	["getNodeValue\("+node+","+field+"\)", Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['edge_get_value'] = function(block) {
	//Get the name of the given edge
	var edge =block.getFieldValue('EDGE_NAME');
	//Get the field value
	var field = Blockly.JavaScript.valueToCode(block, 'FIELD', Blockly.JavaScript.ORDER_ADDITION) || '0';
	return 	["getEdgeValue\("+edge+","+field+"\)", Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['graph_colour_var'] = function(block) {
	//Get the variable name
	var id = Blockly.JavaScript.valueToCode(block, 'VARIABLE', Blockly.JavaScript.ORDER_ADDITION) || 'LOOP';
	
	//Get the colour
	var colour = block.getFieldValue('COLOUR_NAME');
	return "varColour\("+id+",'"+colour+"'\);\n";
};

Blockly.JavaScript['graph_list_iteration'] = function(block) {
  // Repeat n times.
  if (block.getField('ITERATIONS')) {
    // Internal number.
    var repeats = String(Number(block.getFieldValue('ITERATIONS')));
  } else {
    // External number.
    var repeats = Blockly.JavaScript.valueToCode(block, 'ITERATIONS',
        Blockly.JavaScript.ORDER_ASSIGNMENT) || '0';
  }
  //Get the list
  list = Blockly.JavaScript.valueToCode(block, 'LIST', Blockly.JavaScript.ORDER_ADDITION) || '0';
  itFunc = "iterateThroughList\("+list+"\);";
  var branch = itFunc;
  branch = Blockly.JavaScript.addLoopTrap(branch, block.id);
  var code = '';
  var loopVar = Blockly.JavaScript.variableDB_.getDistinctName(
      'count', Blockly.Variables.NAME_TYPE);
  var endVar = repeats;
  if (!repeats.match(/^\w+$/) && !Blockly.isNumber(repeats)) {
    var endVar = Blockly.JavaScript.variableDB_.getDistinctName(
        'repeat_end', Blockly.Variables.NAME_TYPE);
    code += 'var ' + endVar + ' = ' + repeats + ';\n';
  }
  code += 'for (var ' + loopVar + ' = 0; ' +
      loopVar + ' < ' + endVar + '; ' +
      loopVar + '++) {\n' +
      branch + '}\n';
  return code;
};

Blockly.JavaScript['graph_list_loop'] = function(block) {
// Repeat n times.
  if (Blockly.JavaScript.valueToCode(block, 'LIST', Blockly.JavaScript.ORDER_ADDITION) || '0' != 0) {
    // Internal number.
    var list = Blockly.JavaScript.valueToCode(block, 'LIST', Blockly.JavaScript.ORDER_ADDITION) || '0';
	list = list.split(",");
  }
  else{
	  list = [];
  }
  //Get the name of the variable in VARIABLE
  var vari = Blockly.JavaScript.valueToCode(block, 'VAR', Blockly.JavaScript.ORDER_ADDITION) || '0';
  var branch = Blockly.JavaScript.statementToCode(block, 'CODE');
  branch = Blockly.JavaScript.addLoopTrap(branch, block.id);
  var code = '';
  var loopVar = Blockly.JavaScript.variableDB_.getDistinctName(
      'count', Blockly.Variables.NAME_TYPE);
  var listVar = Blockly.JavaScript.variableDB_.getDistinctName(
      'list', Blockly.Variables.NAME_TYPE);
  code += "var "+listVar+" = "+ list.toString()+";\n"+'for (var count in '+listVar+' ) {\nvar ' + vari+" = "+listVar+"[count];\n" +
      branch + '}\n';
  return code;
};

Blockly.JavaScript['graph_choose_next'] = function(block) {
	//Get the highest or lowest value
	op = block.getFieldValue('CHOICE');
	//Get the value the user wants to get
	field = block.getFieldValue('VALUE');
	//Get the list attached
	var list = Blockly.JavaScript.valueToCode(block, 'LIST', Blockly.JavaScript.ORDER_ADDITION) || '0';

	return["nextEdge\('"+op+"','"+field+"',"+list+"\)", Blockly.JavaScript.ORDER_ATOMIC]
};

Blockly.JavaScript['graph_node_edges'] = function(block) {
		//Get the node number
		node = block.getFieldValue('NODE_NAME');
		//Make a list of all the adjecent edges
		list = [];
		//For every edge, check if the node number is in it's 'to' or 'from
		removePrimatives();
		for(var i in edges._data){
			if(parseInt(edges._data[i].to) == node || parseInt(edges._data[i].from) == node){
				//Add it to the list
				list.push("'Edge "+edges._data[i].id+"'");
			}
		}
		return ["["+list.toString()+"]",Blockly.JavaScript.ORDER_ATOMIC];
}

Blockly.JavaScript['graph_plug_edges'] = function(block) {	
		//Get the node number
		node = Blockly.JavaScript.valueToCode(block, 'NODE_NAME', Blockly.JavaScript.ORDER_ADDITION) || '0';
	return ["plugEdges\("+node+"\).split(',')", Blockly.JavaScript.ORDER_ATOMIC];

};

Blockly.JavaScript['graph_plug_nodes'] = function(block) {	
		//Get the node number
		node = Blockly.JavaScript.valueToCode(block, 'NODE_NAME', Blockly.JavaScript.ORDER_ADDITION) || '0';
	return ["plugNodes\("+node+"\).split(',')", Blockly.JavaScript.ORDER_ATOMIC];

};

Blockly.JavaScript['graph_isNode'] = function(block) {	
		//Get the node number
		node = Blockly.JavaScript.valueToCode(block, 'NODE_NAME', Blockly.JavaScript.ORDER_ADDITION) || '0';
		return ["isNode\("+node+"\)", Blockly.JavaScript.ORDER_ATOMIC];

};

Blockly.JavaScript['graph_opposite_node'] = function(block) {	
		//Get the node number
		node = Blockly.JavaScript.valueToCode(block, 'NODE_NAME', Blockly.JavaScript.ORDER_ADDITION) || '0';
		edge = Blockly.JavaScript.valueToCode(block, 'EDGE_NAME', Blockly.JavaScript.ORDER_ADDITION) || '0';
		return ["nextOnEdge\("+node+","+edge+"\)", Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['comment'] = function(block) {
	comment = block.getFieldValue('COMMENT');
	return "//"+comment+";\n";
};

Blockly.JavaScript['return'] = function(block) {
	value = Blockly.JavaScript.valueToCode(block, 'VALUE', Blockly.JavaScript.ORDER_ADDITION) || '0';
	return "return " + value+";\n";
};

Blockly.JavaScript['graph_change_label'] = function(block) {	
		//Get the node number
		node = Blockly.JavaScript.valueToCode(block, 'NODE_NAME', Blockly.JavaScript.ORDER_ADDITION) || '0';
		label = Blockly.JavaScript.valueToCode(block, 'LABEL_GET', Blockly.JavaScript.ORDER_ADDITION) || '0';
		return "changeLabel\("+node+","+label+"\);\n";
};

Blockly.JavaScript['graph_highlight_plug'] = function(block) {	
		//Get the node number
		node = Blockly.JavaScript.valueToCode(block, 'NODE_NAME', Blockly.JavaScript.ORDER_ADDITION) || '0';
		return "highlightPlug\("+node+");\n";
};

Blockly.JavaScript['graph_colour_number'] = function(block) {	
		//Get the node number
		id = Blockly.JavaScript.valueToCode(block, 'NODE_NAME', Blockly.JavaScript.ORDER_ADDITION) || '0';
		num = Blockly.JavaScript.valueToCode(block, 'NUMBER_GET', Blockly.JavaScript.ORDER_ADDITION) || '0';

		return "colourNumber\("+id+","+num+"\);\n";
};

Blockly.JavaScript['edge_add_plug'] = function(block) {	
		//Get the node number
		Nodeone = Blockly.JavaScript.valueToCode(block, 'NODE_ONE', Blockly.JavaScript.ORDER_ADDITION) || '0';
		Nodetwo = Blockly.JavaScript.valueToCode(block, 'NODE_TWO', Blockly.JavaScript.ORDER_ADDITION) || '0';
		return "addEdge\("+Nodeone+","+Nodetwo+"\);\n";
};

Blockly.JavaScript['edge_remove_plug'] = function(block) {	
		//Get the node number
		edge = Blockly.JavaScript.valueToCode(block, 'EDGE_NAME', Blockly.JavaScript.ORDER_ADDITION) || '0';
		return 	"removeEdge\("+edge+"\);\n";
};

Blockly.JavaScript['node_remove_plug'] = function(block) {	
		//Get the node number
		node = Blockly.JavaScript.valueToCode(block, 'Node_NAME', Blockly.JavaScript.ORDER_ADDITION) || '0';
		return 	"removeNode\("+node+"\);\n";
};

Blockly.JavaScript['graph_value_plug'] = function(block) {
	//Get the name of the given edge
	var id = Blockly.JavaScript.valueToCode(block, 'ID', Blockly.JavaScript.ORDER_ADDITION) || '0';
	//Get the field value
	var field = Blockly.JavaScript.valueToCode(block, 'FIELD', Blockly.JavaScript.ORDER_ADDITION) || '0';
	return 	["getValue\("+id+","+field+"\)", Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['graph_value_plug_edge'] = function(block) {
	//Get the name of the given edge
	var id = Blockly.JavaScript.valueToCode(block, 'ID', Blockly.JavaScript.ORDER_ADDITION) || '0';
	//Get the field value
	var field = Blockly.JavaScript.valueToCode(block, 'FIELD', Blockly.JavaScript.ORDER_ADDITION) || '0';
	return 	["getValue\("+id+","+field+"\)", Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['set_value_plug'] = function(block) {
	//Get the name of the given edge
	var id = Blockly.JavaScript.valueToCode(block, 'ID', Blockly.JavaScript.ORDER_ADDITION) || '0';
	//Get the field value
	var field = Blockly.JavaScript.valueToCode(block, 'FIELD', Blockly.JavaScript.ORDER_ADDITION) || '0';
	var value = Blockly.JavaScript.valueToCode(block, 'VALUE', Blockly.JavaScript.ORDER_ADDITION) || '0';
	return 	"setValue\("+id+","+field+","+value+"\);\n";
};

Blockly.JavaScript['set_value_plug_edge'] = function(block) {
	//Get the name of the given edge
	var id = Blockly.JavaScript.valueToCode(block, 'ID', Blockly.JavaScript.ORDER_ADDITION) || '0';
	//Get the field value
	var field = Blockly.JavaScript.valueToCode(block, 'FIELD', Blockly.JavaScript.ORDER_ADDITION) || '0';
	var value = Blockly.JavaScript.valueToCode(block, 'VALUE', Blockly.JavaScript.ORDER_ADDITION) || '0';
	return 	"setValue\("+id+","+field+","+value+"\);\n";
};

Blockly.JavaScript['graph_node_plug'] = function(block) {
	var id = Blockly.JavaScript.valueToCode(block, 'ID', Blockly.JavaScript.ORDER_ADDITION) || '0';
	return 	["\"'\"+String('Node '+"+id+")+\"'\"", Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['graph_edge_plug'] = function(block) {
	var id = Blockly.JavaScript.valueToCode(block, 'ID', Blockly.JavaScript.ORDER_ADDITION) || '0';
	return 	["'Edge "+id+"'", Blockly.JavaScript.ORDER_ATOMIC];
};