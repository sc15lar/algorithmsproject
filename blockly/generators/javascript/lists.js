/**
 * @license
 * Visual Blocks Language
 *
 * Copyright 2012 Google Inc.
 * https://developers.google.com/blockly/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @fileoverview Generating JavaScript for list blocks.
 * @author fraser@google.com (Neil Fraser)
 */
'use strict';

goog.provide('Blockly.JavaScript.lists');

goog.require('Blockly.JavaScript');


Blockly.JavaScript['lists_create_empty'] = function(block) {
  // Create an empty list.
  return ['[]', Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['lists_create_with'] = function(block) {
  // Create a list with any number of elements of any type.
  var code = new Array(block.itemCount_);
  for (var n = 0; n < block.itemCount_; n++) {
    code[n] = Blockly.JavaScript.valueToCode(block, 'ADD' + n,
        Blockly.JavaScript.ORDER_COMMA) || 'null';
  }
  code = '[' + code.join(', ') + ']';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['lists_repeat'] = function(block) {
  // Create a list with one element repeated.
  var functionName = Blockly.JavaScript.provideFunction_(
      'listsRepeat',
      [ 'function ' + Blockly.JavaScript.FUNCTION_NAME_PLACEHOLDER_ +
          '(value, n) {',
        '  var array = [];',
        '  for (var i = 0; i < n; i++) {',
        '    array[i] = value;',
        '  }',
        '  return array;',
        '}']);
  var argument0 = Blockly.JavaScript.valueToCode(block, 'ITEM',
      Blockly.JavaScript.ORDER_COMMA) || 'null';
  var argument1 = Blockly.JavaScript.valueToCode(block, 'NUM',
      Blockly.JavaScript.ORDER_COMMA) || '0';
  var code = functionName + '(' + argument0 + ', ' + argument1 + ')';
  return [code, Blockly.JavaScript.ORDER_FUNCTION_CALL];
};

Blockly.JavaScript['lists_length'] = function(block) {
  // String or array length.
  var argument0 = Blockly.JavaScript.valueToCode(block, 'VALUE',
      Blockly.JavaScript.ORDER_FUNCTION_CALL) || '[]';
  return [argument0 + '.length', Blockly.JavaScript.ORDER_MEMBER];
};

Blockly.JavaScript['lists_isEmpty'] = function(block) {
  // Is the string null or array empty?
  var argument0 = Blockly.JavaScript.valueToCode(block, 'VALUE',
      Blockly.JavaScript.ORDER_MEMBER) || '[]';
  return ['!' + argument0 + '.length', Blockly.JavaScript.ORDER_LOGICAL_NOT];
};

Blockly.JavaScript['lists_indexOf'] = function(block) {
  // Find an item in the list.
  var operator = block.getFieldValue('END') == 'FIRST' ?
      'indexOf' : 'lastIndexOf';
  var argument0 = Blockly.JavaScript.valueToCode(block, 'FIND',
      Blockly.JavaScript.ORDER_NONE) || '\'\'';
  var argument1 = Blockly.JavaScript.valueToCode(block, 'VALUE',
      Blockly.JavaScript.ORDER_MEMBER) || '[]';
  var code = argument1 + '.' + operator + '(' + argument0 + ') + 1';
  return [code, Blockly.JavaScript.ORDER_ADDITION];
};

Blockly.JavaScript['lists_getIndex'] = function(block) {
  	var listVal = Blockly.JavaScript.variableDB_.getName(block.getFieldValue('VAR'), Blockly.Variables.NAME_TYPE);
	var  index = Blockly.JavaScript.valueToCode(block, 'NUM', Blockly.JavaScript.ORDER_ADDITION) || '0';
	return[listVal+"["+index+"]",Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['lists_killIndex'] = function(block) {
  	var listVal = Blockly.JavaScript.variableDB_.getName(block.getFieldValue('VAR'), Blockly.Variables.NAME_TYPE);
	var  index = Blockly.JavaScript.valueToCode(block, 'NUM', Blockly.JavaScript.ORDER_ADDITION) || '0';
	return listVal+".splice("+index+"-1, 1 );\n";
};

Blockly.JavaScript['lists_setIndex'] = function(block) {
	var listVal = Blockly.JavaScript.variableDB_.getName(block.getFieldValue('VAR'), Blockly.Variables.NAME_TYPE);
	var num = Blockly.JavaScript.valueToCode(block, 'NUM', Blockly.JavaScript.ORDER_ADDITION) || '0';
	var input = Blockly.JavaScript.valueToCode(block, 'INPUT', Blockly.JavaScript.ORDER_ADDITION) || '0';
  return listVal+"["+num+"] = "+input+";\n";
};

Blockly.JavaScript['lists_sort'] = function(block) {
  // Block for sorting a list.
  var listCode = Blockly.JavaScript.valueToCode(
      block, 'LIST',
      Blockly.JavaScript.ORDER_FUNCTION_CALL) || '[]';
  var direction = block.getFieldValue('DIRECTION') === '1' ? 1 : -1;
  var type = block.getFieldValue('TYPE');
  var getCompareFunctionName = Blockly.JavaScript.provideFunction_(
          'listsGetSortCompare',
  ['function ' + Blockly.JavaScript.FUNCTION_NAME_PLACEHOLDER_ +
    '(type, direction) {',
      '  var compareFuncs = {',
      '    "NUMERIC": function(a, b) {',
      '        return parseFloat(a) - parseFloat(b); },',
      '    "TEXT": function(a, b) {',
      '        return a.toString() > b.toString() ? 1 : -1; },',
      '    "IGNORE_CASE": function(a, b) {',
      '        return a.toString().toLowerCase() > ' +
      'b.toString().toLowerCase() ? 1 : -1; },',
      '  };',
      '  var compare = compareFuncs[type];',
      '  return function(a, b) { return compare(a, b) * direction; }',
      '}']);
  return ['(' + listCode + ').slice().sort(' +
      getCompareFunctionName + '("' + type + '", ' + direction + '))',
      Blockly.JavaScript.ORDER_FUNCTION_CALL];
};

Blockly.JavaScript['lists_split'] = function(block) {
  // Block for splitting text into a list, or joining a list into text.
  var value_input = Blockly.JavaScript.valueToCode(block, 'INPUT',
      Blockly.JavaScript.ORDER_MEMBER);
  var value_delim = Blockly.JavaScript.valueToCode(block, 'DELIM',
      Blockly.JavaScript.ORDER_NONE) || '\'\'';
  var mode = block.getFieldValue('MODE');
  if (mode == 'SPLIT') {
    if (!value_input) {
      value_input = '\'\'';
    }
    var functionName = 'split';
  } else if (mode == 'JOIN') {
    if (!value_input) {
      value_input = '[]';
    }
    var functionName = 'join';
  } else {
    throw 'Unknown mode: ' + mode;
  }
  var code = value_input + '.' + functionName + '(' + value_delim + ')';
  return [code, Blockly.JavaScript.ORDER_FUNCTION_CALL];
};

Blockly.JavaScript["lists_is_in"] = function(block) {	
		var item = Blockly.JavaScript.valueToCode(block, 'ITEM_NAME', Blockly.JavaScript.ORDER_ADDITION) || '0';
		var list = Blockly.JavaScript.valueToCode(block, 'LIST_NAME', Blockly.JavaScript.ORDER_ADDITION) || '0';
		return ["isIn\("+item+","+list+"\)", Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript["lists_merge"] = function(block) {	
		var one = Blockly.JavaScript.valueToCode(block, 'LISTONE', Blockly.JavaScript.ORDER_ADDITION) || '0';
		var two = Blockly.JavaScript.valueToCode(block, 'LISTTWO', Blockly.JavaScript.ORDER_ADDITION) || '0';
		return [one+".concat("+two+")", Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript["lists_add_to"] = function(block) {	
		var item = Blockly.JavaScript.valueToCode(block, 'ITEM_NAME', Blockly.JavaScript.ORDER_ADDITION) || '0';
		var list = Blockly.JavaScript.valueToCode(block, 'LIST_NAME', Blockly.JavaScript.ORDER_ADDITION) || '0';
		return ["addToList\("+item+","+list+"\).split(',')", Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript["lists_difference2"] = function(block) {	
		var one = Blockly.JavaScript.valueToCode(block, 'ONE_NAME', Blockly.JavaScript.ORDER_ADDITION) || '0';
		var two = Blockly.JavaScript.valueToCode(block, 'TWO_NAME', Blockly.JavaScript.ORDER_ADDITION) || '0';
		return ["listDifference\("+one+","+two+"\).split(',',-1)", Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript["lists_difference"] = function(block) {
		var listVal = Blockly.JavaScript.variableDB_.getName(block.getFieldValue('VAR'), Blockly.Variables.NAME_TYPE);	
		var one = Blockly.JavaScript.valueToCode(block, 'ONE_NAME', Blockly.JavaScript.ORDER_ADDITION) || '0';
		var two = Blockly.JavaScript.valueToCode(block, 'TWO_NAME', Blockly.JavaScript.ORDER_ADDITION) || '0';
		return listVal+" = listDifference\("+one+","+two+"\).split(',');\n"+ "if\("+listVal+"[0] == ''\){\n"+listVal+" = new Array\(\);\n}\n";
};

Blockly.JavaScript["lists_reverse_list"] = function(block) {	
		var list = Blockly.JavaScript.valueToCode(block, 'LIST', Blockly.JavaScript.ORDER_ADDITION) || '0';
		return ["reverseList\("+list+"\).split(',')",Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['lists_getSublist'] = function(block){
		var listVal = Blockly.JavaScript.variableDB_.getName(block.getFieldValue('VAR'), Blockly.Variables.NAME_TYPE);
		var  start = Blockly.JavaScript.valueToCode(block, 'FROM', Blockly.JavaScript.ORDER_ADDITION) || '0';
		var  end = Blockly.JavaScript.valueToCode(block, 'TO', Blockly.JavaScript.ORDER_ADDITION) || '0';
		return[listVal+".slice("+start+","+end+")",Blockly.JavaScript.ORDER_ATOMIC];
};