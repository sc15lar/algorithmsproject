/**
 * @license
 * Algorithm visualizer
 *
 * Copyright 2016 University of Leeds.
 * https://www.leeds.ac.uk/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

/**
 * @fileoverview Generating JavaScript for object blocks.
 * @author Luke Roberts
 */

Blockly.JavaScript['new_object'] = function(block) {
	return ["{}",Blockly.JavaScript.ORDER_ATOMIC];
}; 
 
  Blockly.JavaScript['object_value_get'] = function(block) {
	 var obj =  Blockly.JavaScript.variableDB_.getName(block.getFieldValue('VAR'), Blockly.Variables.NAME_TYPE);
	 var field =  Blockly.JavaScript.valueToCode(block, 'FIELD', Blockly.JavaScript.ORDER_NONE) || '\'\'';
	return [obj+"["+field+"]",Blockly.JavaScript.ORDER_ATOMIC];
}; 
 
 Blockly.JavaScript['object_access'] = function(block) {
	 var obj =  Blockly.JavaScript.variableDB_.getName(block.getFieldValue('VAR'), Blockly.Variables.NAME_TYPE);
	 var field =  Blockly.JavaScript.valueToCode(block, 'FIELD', Blockly.JavaScript.ORDER_NONE) || '\'\'';
	 var value =  Blockly.JavaScript.valueToCode(block, 'VALUE', Blockly.JavaScript.ORDER_NONE) || '\'\'';
	return obj+"["+field+"] = "+value+";\n";
}; 