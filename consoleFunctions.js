/**
 * @license
 * Algorithm visualizer
 *
 * Copyright 2016 University of Leeds.
 * https://www.leeds.ac.uk/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

 /**
 * @fileoverview Functions that control the embedded console
 * in the algorithm visualizer.
 * @author lukeadamroberts@gmail.com (Luke Roberts)
 **/

/**
* Event listener that prints some default text to the
* console and focuses the window to italics
**/
$(document).ready(function(){
	printToConsole("ALGORITHM VISUALIZER VERSION 1.1<br>MODIFICATION PROTECTED BY THE APACHE 2 LICENCE <a>http://www.apache.org/licenses/LICENSE-2.0</a><br>");
	document.getElementById("userInput").focus();
});

/**
* consolePopOut() function
* Handles the removal of the console and it's resizing in
* a pop-out window.
**/
function consolePopOut(){
	popList.console.dom.userString = userString;
	popList.console.dom.addEventListener("keydown",function(e){keyTest(e);});

	setTimeout(function(){
		$('#console',popList.console.dom.document).css('width','100%');
		$('#console',popList.console.dom.document).css('height','100%');
		$('#consolePopOut',popList.console.dom.document).css('display','none');
	},1000);
}

/**
* consolePopIn() function
* Handles the re-intergration of the console into the main
* page.
**/
function consolePopIn(){
		$("#userInput").on('keydown', function(e){
			keyTest(e);
		});
		$('#consolePopOut').css('display','block');
}

/**
* keyTest() function
* Checks what key has been pressed while the
* console is selected.
**/
function keyTest(event){
	var input = popList.console.dom.document.getElementById('userInput');
	if(event.which == 13){
		userString = $(input).html();
		event.cancelBubble = true;
		event.returnValue = false;
		setTimeout(function(){$(input).html("");},1);
		input.focus();
	}
	else if(event.which == 38){
		$(input).html(currString);
	}
}

/**
* printToConsole() function
* Prints the given text into the console
**/
function printToConsole(text){
	var console = popList.console.dom.document.getElementById("console");
	var answerBox = popList.console.dom.document.getElementById("answerBox");
	var line = popList.console.dom.document.createElement("p");
	line.innerHTML = text;
	console.insertBefore(line,answerBox);
}

/**
* consoleEx() function
* Considers the command given by the userAgent
* and performs the appropriate command
**/
function consoleEx(){
	var command = userString;
	if(userString == "ahmen"){
		printToConsole("ZZZZZ$$$$$$7777777IIIIIIIII????????????????????????IIIIIIIIII777777$$$$$ZZZZZZOO<br>ZZZZ$$$$$$777777IIIIIII???????????????????????????????IIIIIIIIII77777$$$$$ZZZZZZ<br>ZZ$$$$$777777IIIIII?????????????++++++++?++++?????????????IIIIIIII7777$$$$$$ZZZZ<br>Z$$$$77777IIIIIII????????+++++++++++++++++++++++++++??????????IIIIII77777$$$$ZZZ<br>$$$$7777IIIIII????????+++++++++++++++++++++???++++++++++?????????IIIII7777$$$$ZZ<br>$$$7777IIIII???????+++++++++++=+=++IO888Z88OO88D8?+++++++++???????IIIII7777$$$$$<br>$7777IIIIII??????++++++++======+=7ZO8DNDD8OO88DDDND8?++++++++???????IIIII7777$$$<br>$777IIIII?????++++++++=========?OZZ8DDDD8OZO8DDDDDDDN8+=+++++++++????IIIII7777$$<br>777IIII?????+++++++========~==88OZOZ8D8O8OZOO8DDDNNDNNND===+++++++??????IIII7777<br>77III??????++++++=========~==8OOOZZ888ZZOZZO8888DNNNDNNDN++==+++++++?????IIII777<br>7III?????+++++=========~~~=~O8D8O$ZZ$I??I7ZOOODDNNNNNNNNNN+====++++++?????IIII77<br>III????+++++=========~~~~~~OD8D8D$7+======+?I7$ODNNNNNNDMNN======++++++????IIIII<br>II???++++++========~~~~~~~$8D8D8$+==~=~~==+++?II$8NMMNNNNMNO=======++++++???IIII<br>I????+++++=======~~~~~~~~+DD8DI+=~~~~~~~=====+??I7ONNMNNNMMNI~=======++++????III<br>???+++++=======~~~~~~~~~~88DZ+==~~~~~~~~~====++??I$ONMMNNNMNN==~======+++++???II<br>???++++======~~~~~~~~:~~=88Z+===~~~~~~~~~====+++?I7$OMNMNNMMM7~=~=======++++????<br>??++++======~~~~~~~~~~~~?OO$====~~~~~~~=======++?I77ZDMMNNNMMO=~~=======++++????<br>?++++======~~~~~~~~~~~=?$88?==~~=~~~~~~~===~~=++?I77$ODMNMNMMM==~~~======++++???<br>++++======~~~~~~~~~~~:~~$8D==+I?I$$Z7?=====~=+??I777$Z88MNMMMMI~~~~~======++++??<br>++++=====~~~~~~~~~:::~:~?8O~=+?I7III777?=+==?I7$ZOZ$$OO8DMMMMMD?~~~~~======++++?<br>++======~~~~~~~::::::=++7ZI=~==+?7ZO$???===IZ888OO88DD888MMMMMNO~~~~~~======++++<br>+======~~~~~~::::::::=:==I?~~~~==+I$II==~~+8D8ZZOD88OO8O8NMNMMDD~~~~~~~~=====+++<br>+=====~~~~~~:::::::::::=+++~:~~~~=+?+===~~?88O7I7$Z8OOOO8NNNNMDD?~~~~~~~=====+++<br>=====~~~~~~::::::::::::=~~=~~::::~~~=~~~~=ZOOZ$777$77ZOO8MNDMNND7~~~~~~~~=====++<br>====~~~~~~:::::::::::::~~:~=~~:::::~~~~~~?OOO7II????7$O8DNN8MNNDZ=~~~~~~~~====++<br>===~~~~~~~:::::::::::::~Z~~~:~:::::~~~=~~IZ8ZI?????I$Z88NDDNMNDDO~~~~~~~~~====++<br>===~~~~~~::::::::::::::,$Z$+~~~~~+?=~~~:=+$Z8$??+?I7ZO8DDDDMMNNDD+=~~~~~~~======<br>==~~~~~~::::::::::::::::$ZO?~==+I+~~=?~~=+ZOOZ$7?I7ZO8DDDMMMMNND8I~~~~~~~~======<br>=~~~~~~::::::::::::::::=$$O7~~?+=~~~~~+IZO8OZZ$$77$888MMMMMMMNDD8Z~~~~~~~~~=====<br>=~~~~~~:::::::::::::::~+$$OD=~~==I~~:~=+=?$$$Z$Z$77DODMMMMMMMDD88O=~~~~~~~~=====<br>~~~~~~::::::::::::::::~$$ZO8+~==~:+7?I7II7ZZ8D$77?$O8MMMMMMMMND88Z7~~~~~~~~=====<br>~~~~~::::::::::::::::,?ZZO88N+=~~~=~II=++7ZZ$$7$II88DMMMMMMMMND88OZ+~~~~~~~=====<br>~~~~~:::::::::::::::::$ZZO8DNI==~~~=?+++=?I$Z$ZZI88DNMMMMMMMMN888OO7=~~~~~~~====<br>~~~~~::::::::::::::,::ZZZO8DDZ===~~==?I77$Z$Z$O$Z8DDNMMMMMMMMD88D8O$+~~~~~~~====<br>~~~~:::::::::::::::::=ZZZODNNN=~==~~~=++??77IZ$ODD88DMMDDMMMN8NNNDOOI=~~~~~~====<br>~~~~::::::::::::::,::IZZOODNNM+~~+=~~~==+?IIZO8D8OOODMMNNMMMN8MNNN8N7?~~~~~=====<br>~~~~::::::::::::::,::$ZZOODNNNI~~~+?++???IZO88OOZ$$O8NMNNMMMNMMMMNDDO?+~~~~~====<br>~~~~:::::::::::::,::~OZZOO8NMMD?~~~=?7$$ZZZZZ$7777$Z8NDNMMMMMMNNMMN8NIII~~~~====<br>~~~~::::::::::::::::7OZZO8DMMMMO=~~~~=++??I77IIIII7$OD8DDNNDNNMNNMMDN8=+==~=====<br>~~~::::::::::::::::~OZZZO8NMMMDN+~~~~~~~~=+????+??I7$8DDD8DDD8DNDMMNNNI=I=======<br>~~~::::::::::::::::7OZO8OOMMMDDM?=~==~~~=++?+==+++??7O8DD88D8O88DMMMMDZ+~?~=====<br>~~~:::::::::::::::~OZZOO8NMN8DN8?+=~===+??++=====+++Z8888888888ODDD8DDNZ==+=====<br>~~~::::::::::::::~?$ZOOODMDODMD$$~=~==++++++=~=~==+OO888OOOO88OODD888888D?+?====<br>~~~::::::::::,::~IZZZZ88MNO8DNZ$Z:=~~==+++?======IZOOO88OOO888O8DDDDD8888O8ZI==+<br>~~~::::::::::~I7I$$ZO88NMOO8NN$77?~=~====+?+===?OZZOOOOO8O8DOO8DD88DDDDD8888887?<br>~~~::::::::~$$77$$$ZZONMOZOND87$$7I~~~==+==~+Z$ZZ$OO8OOOO8OOO8Z8NN88DDDD8D8DD888<br>~~~::::::~~7Z$7$$Z$Z8DMDZO8DNO77$$Z7?==~+7OZZ$$Z7Z8OOOOOOOZZZO88MN8888DDDDDDDDD8<br>~~~::::::=O$$$ZZ$$ZO8NOOO8DDN8777$$$$ZZZ$$$$Z$77OOOOOZZOOO8D8N$OMN888DDDDDDDDDDD<br>~~~:~~:~+OZ$$OZ$$ZZ88DO888DDDN77I77$$$$$Z$$$$7$OOZZ88D88D8Z$8MMMND88DD88DD88DDDD<br>~~~~~~~IZO$ZOO$$$ZOO8D8OD8DDDNO7777777$$7$$7$8888OZ$7ZONMMNNNNDDD88DDD8DDD88DDDN<br>~~~~::ZOOOZ$OZZ$ZZ88M88D888DDDDZ7777777777I$DNNNNMMMMMMNNNNDDDDD88DDD88DDD8DDDDD<br>~~~~:=O8OZ$ZOOZ$OOOOMMM8Z8888NN8777777777ONMMMNNNDDNNDDDDDDDDDDD88NND8DDD88DDDDD<br>~~~~~$O8OZO8OZZZOOOZMMMNNDZODDNN777777I7NNNNNDDDD8DND8888DDD88888DMD8DD888DDDDND<br>~~~~:88OOZ88OO$O8O8ZMMNDDDDN8ONNO77777ONNDDND8D88DDD88888DDD8888DNND8D888DDD8NDD<br>~~~~~88OZO888OOD8O8OMMNN888DDDOZDZ7777DNDDDDDD888DND888888D8D888DMN8888DNNDDNNDD<br>~~~~=888OODDDO8D8O88MMND888DDD8D$D$77ZNND8DDD888DND8888888DD8888NMD888DNN88DMDDD<br>~~~~+8888ODNDO8D8O88NMND8888DD8DDDD77DMDDDDD8888DDD8888888DD888DMND8DDMN8DDMNDDN<br>=~~~?D8DD8DNDDDDNOO8MNNDD8888DD88D8O$NDD8DD8888DND88888888D888DNMD8DDMND8DMMDDNN<br>==~~+D8DDD888DDDN888MDNN88888D888DMO8M888D88D8DDDD88888888DD88NMMDDDMM88DNMNDDNM<br>==~~=NDDDD8DND88DD8O8NDDD8888D8D88OZMD88D88D88DDD8888888888D88NMMDDMMD8DNMNDDNMM<br>==~~=DNNDNDDDND8DNNNDDNND888D88888ODDD8D88DD88DDD8888888DDDD8DNMNDNMN8DDMMDDNMMN<br>===~Z8DNDDNDDDDDNNNN88DNDD8888888OZM888DDDDD8DDDD888888DDDDD8DMMNNMN8DDNMNDDNMNN<br>===~D8DNNDDNDDDNNNDNNNDDNDD888888OOD88DDDDDD8DDDD8888888DD888DMMDNNDDDNMMDDNMNNN<br>==~=D8DDNDDDNDDNNNDDDDDDDD8888DD8O888DDDDDD88DDDDDDDDDD8DDDDDDMMDMDDDNMMNDNMNNNN<br>==~I8DDDNNDDDDDDNNDDDDDDDDDDDDDDD8DDDDDDDDD88DDDDDDDDDDDDDDDDDNMNMDNNMMNDDNMNNDD<br>+==O888DDMDDDNNNNNNDDDDDDDD8DDDD8ODDDDNNNDD8DDDDDDDDDDDDO88DDDMMNNDNNMDDDNMNNDDD<br>+==DDD88DNNDDNNNNNNDDDDDDDDDDDDDDDNDDDNNDDD8DDDDDDN$7+==ZDDDDDMMMDNNMNDNNMNNDDDD<br>==+DD888DDMNDNNNNNNDDDDDDDDDDDDDDDDDDNNNDDD8DDDDD8M~7M7$Z$DDDDMMMNNMMDNNMMNNDDNN<br>+==DDD88DNNDDNNNNNNDDDDDDDDDDDDDDDNDDDNNDDD8DDDDDDN$7+==ZDDDDDMMMDNNMNDNNMNNDDDD<br>########################## PRAISE BE TO HIM #####################################");
	}
	else if(userString == "help"){
		printToConsole("THE FOLLOWING COMMANDS CAN BE USED TO CONTROL THE PROGRAM EXECUTION:<br>runCode() - Will begin a run of the currently constructed blocks.<br>stepCode() - Will make the next step through the blocks.<br><br>Other than this, the console is mainly used for input and output of data from the program constructed out of blocks, although the console will respond to most javascript commands<br>Created by Luke Roberts.")
	}
	else if(userString == "aww"){
		window.open("http://brightside.me/wonder-animals/these-ultra-cute-pictures-are-proof-that-dogs-really-are-our-best-friends-119105/");
	}
	else{
		try{
			printToConsole("AV> "+command);
			var result = eval(command);
			if(result != undefined){
			printToConsole(result);
			}
		}
		catch(err){
			printToConsole(err);
		}
	}
}

/**
* getFromConsole() function
* Used by the prompt command to signal that user
* input is needed.
* @args msg: the message printed in the console
* type: what type of variable is required (stored)
* vari: the variable that needs changing.
* @return a default value.
**/
function getFromConsole(msg,type,vari){
	printToConsole(msg);
	popList.console.dom.document.getElementById("userInput").focus();
	waiting = true;
	msgType = type;
	return undefined;
}

/**
* change() function
* Used by the prompt command to ensure that
* the program cannot continue until the user
* has provided input.
* @args variable the needs to be updated with
* user input.
**/
function change(vari){
	changeVar = vari;
	if(waiting == true){
		stopEx = true;
		lock = true;
	}
}
