/**
 * @license
 * Algorithm visualizer
 *
 * Copyright 2016 University of Leeds.
 * https://www.leeds.ac.uk/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/
 
 /**
 * @fileoverview Functions that interact with the graph visualization
 * in the algorithm visualizer. Also some list functions.
 * @author lukeadamroberts@gmail.com (Luke Roberts)
 */

 /**
 * graphPopOut() function
 * Moves the variables across to use with the popOut
 **/
 function graphPopOut(){
	popList.graphBox.dom.network = network;
	popList.graphBox.dom.nodes = nodes;
	popList.graphBox.dom.edges = edges;
	popList.graphBox.dom.data = data;
	//The dom is not properly loaded by this point so set an interval checker to do the job
	sureDraw = setInterval(function(){drawGraphPop();},300);
 }

/**
* drawGraphPop() function
* Keeps attempting to resize the popped-out graph
* until it is loaded.
**/
function drawGraphPop(){
	try{
	draw();
	}
	catch(err){return;}
	clearInterval(sureDraw);
	$('#graphBox',popList.graphBox.dom.document).css('width','100%');
	$('#graphBox',popList.graphBox.dom.document).css('height','100%');
	$('#mynetwork',popList.graphBox.dom.document).css('width','100%');
	$('#mynetwork',popList.graphBox.dom.document).css('height','100%');
	if(popList.graphBox.popped == true){
		$('#graphPopOut',popList.graphBox.dom.document).css('display','none');
	}
	else{
		$('#graphPopOut',popList.graphBox.dom.document).css('display','block');
	}
}

/**
* graphPopIn() function
* Begins a timer that will call a function to resize the graph.
**/ 
function graphPopIn(){
	sureDraw = setInterval(function(){drawGraphPop();},300);draw();
}
 
/**
* destory() function
* Destorys any existing graph.
**/
function destroy() {
   if (network !== null) {
    network.destroy();
    network = null;
    }
}

/**
* draw() function
* Creates a new instance of the graph visualization and
* creates some defaut values.
**/
function draw() {
	var location = popList.graphBox.dom.document;
	destroy();
	if(data == undefined){
            nodes = new vis.DataSet();
            nodes.add([
                {id: '0', label: '0'},
            ]);
            edges = new vis.DataSet();
            edges.add([
            ]);
	  data = {
                nodes: nodes,
                edges: edges
            };
	}
      var container =  location.getElementById('mynetwork');
      var options = {
		interaction:{hover:true},
		height: '100%',
		width: '100%',
        layout: {randomSeed:seed},
        locale: 'en',
		autoResize: true,
		nodes:{
			shadow:{ enabled: false}
		},
        manipulation: { 
		enabled:false
        }
      };
      network = new vis.Network(container, data, options);
	  network.on("hoverNode", function (params) { updateHoverTitles(); });
	  network.on("hoverEdge", function (params) {updateHoverTitles(); });
	  network.fit();
    }

/**
* saveData() function
* Saves the current graph (nodes and edges).
**/	
function saveData(data,callback) {
	var location = popList.graphBox.dom.document;
    data.id = location.getElementById('node-id').value;
    data.label = location.getElementById('node-label').value;
    clearPopUp();
    callback(data);
}
	
/**
* getEdgeList() function
* @return a list of the edges on the graph.
**/
function getEdgeList(){
	var edgeList = network.body.data.edges._data;
	var edges = {};
	for(var i in edgeList){
		var name = edgeList[i].from+","+edgeList[i].to;
		edges[name] = edgeList[i].id;
	}
	return edges;
}

/**
* getNodeList() function
* @return a list of the nodes on the graph.
**/
function getNodeList(){
	var nodeList = [];
	var edgeList = network.body.data.edges._data;
	var graphRecord = {};
	var source = network.body.data.nodes._data;
	for(var i in source){
		nodeList.push(source[i].id);
	}
	for( var n in nodeList){
		graphRecord[nodeList[n]] = {};
	}
	for(var x in edgeList){
		try{
		var start = edgeList[x].from;
		var end = edgeList[x].to;
		var edgeID = edgeList[x].id;
		graphRecord[start][end] = edgeID;
		graphRecord[end][start] = edgeID;
		}
		catch(err)
		{}
	}
	return graphRecord;
}
/**
* clearGraph() function
* Clears the graphs and returns it to the default state.
**/
function clearGraph(){
	var nodeList = Object.keys(nodes._data);
	var edgeList = Object.keys(edges._data);
	for(var i in nodeList){
		resetNode(nodeList[i]);
	}
	for(var n in edgeList){
		resetEdge(edgeList[n]);
	}
	backup.nodes = $.extend(true,{},nodes);
	backup.edges = $.extend(true,{},edges);
	network.redraw();
}

/**
* highlightNode() function
* Highlights a node in red.
* @arg The node to be highlighted
**/
function highlightNode(node){
	resetGraph();
	nodes.update({
           id: node,
           color: {
			background: "#ff0066"
		},
		font:{
			color: '#ffffff'
		}
    });
}

/**
* resetNode() function
* Resets a node to it's default colour.
* @arg The node to be reset
**/	
function resetNode(node){
	nodes.update({
        id: node,
        color: {
			background: "#97C2FC"
		},
		font:{
			color: '#000000'
		}
    });
}
	
/**
* addNode() function
* Adds a new node with the ID and label
* of the new number of nodes.
**/
function addNode() {
	high = 0;
	 for(var i in nodes._data){
		 if(parseInt(nodes._data[i].id) > high){
			high = parseInt(nodes._data[i].id);
		 }
	 }
       nodes.add({
		id: high+1,
		label: nodes.length
	});
	nodes.values[high+1] = {id:high+1};
}

/**
* removeNode() function
* @arg The node to be removed.
**/	
function removeNode(node) {
	nodes.remove({id: node});
}

/**
* addEdge() function
* Adds an edge between two nodes.
* @args the two nodes to place an edge between.
**/		
function addEdge(Nodeone,Nodetwo) {
   if(Nodeone.search("Node") != -1){Nodeone = Nodeone.replace(/\D/g,'');};
   if(Nodetwo.search("Node") != -1){Nodetwo = Nodetwo.replace(/\D/g,'');};
   high = 0;
   for(var i in edges._data){
	   if(parseInt(edges._data[i].id) > high){
		   high = parseInt(edges._data[i].id);
	   }
   }
	edges.add({
	id: high + 1,
	from: Nodeone,
	to: Nodetwo
	});
	edges.values[high+1] = {id:high+1};
}
   
/**
* highlightEdge() function
* @arg The edge to highlight red.
**/
function highlightEdge(edge) {
	resetGraph();
    edges.update({
		id: edge,
		color: {
			color: "#ff0066"
		}
    });
}
	 
/**
* resetEdge() function
* Resets and edge to it's default colour.	 
* @arg The edge to be reset.
**/
function resetEdge(edge){
	edges.update({
        id: edge,
        color: {
			color: "#848484"
		}	
    });
}

/**
* removeEdge() function
* Removes an edge from the graph.
* @arg The edge to remove.
**/	 
function removeEdge(edge) {
	if(edge.search("Edge") != -1){
		edge = edge.substring(6,edge.length-1)
	}
	edges.remove({id: edge});
}
		
/**
* nodeColour() function
* Changes a nodes colour.
* @args id: The node to be changed, colour: The
* new colour of the node.
**/
function nodeColour(id, colour){
	nodes.update({ id: id, color: { background: colour}, font:{color : getContrast(colour)}});
	backup.nodes._data[id].color.background = colour;
	backup.nodes._data[id].font.color = getContrast(colour);
}

/**
* edgeColour() function
* Changes an edge's colour.
* @args id: The edge to be changed, colour: The
* new colour of the edge.
**/
function edgeColour(id, colour){
	edges.update({ id: id, color: { color: colour}});
	backup.edges._data[id].color.color = colour;
}
	
/**
* nodeColour() function
* Considers the type of the variable being past to
* it and calls a function to change the colour.
* @args id: The variable to be changed, colour: The
* new colour of the variable.
**/
function varColour(id,colour){
	if(id.search("Node") != -1){
		id = id.substring(5,id.length);
		nodeColour(id,colour);
	}
	else{
		id = id.substring(5,id.length);
		edgeColour(id,colour);
	}
}
	
/**
* resetGraph() function
* Resets all the nodes and edges on the graph
* to their default colours.
**/
function resetGraph(){
	for(var x in backup.nodes._data){
		nodes.update({ id: backup.nodes._data[x].id, color: { background: backup.nodes._data[x].color.background},font:{ color: backup.nodes._data[x].font.color}});
	}
	for(var y in backup.edges._data){
		edges.update({ id: backup.edges._data[y].id, color: { color: backup.edges._data[y].color.color}});
	}
}
	
/**
* setNodeValue() function
* Changes a value stored on a node.
* @args id: the node to hold the value,
* field: the field to be changed, value:
* the new value.
**/	
function setNodeValue(id,field,value){
	nodes.values[id][field] = value;
}
/**
* getNodeValue() function
* Returns to a value from a node
* @args id: the node to be checked,
* field: the field to be checked for value.
* @return the value of the field.
**/
function getNodeValue(id,field){
	return nodes.values[id][field];
}
/**
* setEdgeValue() function
* Changes a value stored on an edge.
* @args id: the edge to hold the value,
* field: the field to be changed, value:
* the new value.
**/	
function setEdgeValue(id,field,value){
	edges.values[id][field] = value;
}
/**
* getEdgeValue() function
* Returns to a value from an edge
* @args id: the edge to be checked,
* field: the field to be checked for value.
* @return the value of the field.
**/
function getEdgeValue(id,field){
	return edges.values[id][field];
}

/**
* getValue() function
* Considers the type of a variable and calls
* a function to retrieve a value from a field.
* @args id: The variable to be checked, field: the 
* field to be checked for value.
**/	
function getValue(id,field){
	if(id.search("Node") != -1){
		id = id.substring(6,id.length-1);
		return nodes.values[id][field];
	}
	else{
		id = id.substring(6,id.length-1);
		return edges.values[id][field];
	}
}

/**
* setValue() function
* Considers the type of a variable and calls
* a function to set a value from a field.
* @args id: The variable to be checked, field: the 
* field to be given a value, value: the value to be applied.
**/		
function setValue(id,field,value){
	if(id.search("Node") != -1){
		id = id.substring(6,id.length-1);
		nodes.values[id][field] = value;
	}
	else{
		id = id.substring(6,id.length-1);
		edges.values[id][field] = value;
	}
}

/**
* updateHoverTitles() function
* Updates the values that appear when hovering 
* over a node or an edge of the graph.
**/	
function updateHoverTitles(){	
	try{
		for(var x in nodes._data){
			keys = Object.keys(nodes.values[nodes._data[x].id]);
			if(keys.length == 0){
				nodes.update({id: nodes._data[x].id, title:'Empty'});
			}
			else{
				nodes.update({id: nodes._data[x].id, title: JSON.stringify(nodes.values[nodes._data[x].id])});
			}
		}
		for(var y in edges._data){
			keys = Object.keys(edges.values[edges._data[y].id]);
			if(keys.length == 0){
				edges.update({id: edges._data[y].id, title:'Empty'});
			}
			else{
				edges.update({id: edges._data[y].id, title: JSON.stringify(edges.values[edges._data[y].id])});
			}
		}
	}
	catch(err)
	{}
}

/**
* iterateThroughList() function
* Moves through a given list, applying code
* specified in the block.
* @arg The list to be iterated through.
**/
function iterateThroughList(list){
	if(list[0] == "["){list = list.substring(1,list.length-1);}
	if(typeof(list)=='string'){
		list=list.split(",");
	}
	if(iteration==list.length){
		iteration = 0;
	}
	if(list[iteration].search("Node") != -1){
		id = list[iteration].substring(6,list[iteration].length-1);
		highlightNode(id);
	}
	else{
		id = list[iteration].substring(6,list[iteration].length-1);
		highlightEdge(id);
	}
	iteration++;
}

/**
* removePrimatives() functions
* Removes references from a the graph vizualizer object.
* This allows for the graph to be stringifed for saving.
**/	
function removePrimatives(){
	for(var i in edges._data){
		if(edges._data[i].from.data != undefined){
			edges._data[i].from = edges._data[i].from.data;
		}
		if(edges._data[i].to.data != undefined){
			edges._data[i].to = edges._data[i].to.data;
		}
	}
}

/**
* plugEdges() function
* @arg The node to be considered.
* @return a list of edges connected to this node.
**/
function plugEdges(node){
	node = node.substring(6,node.length-1);
	list = [];
	removePrimatives();
	for(var i in edges._data){
		if(parseInt(edges._data[i].to) == node || parseInt(edges._data[i].from) == node){
			list.push("Edge "+edges._data[i].id)
		}
	}
	if(list.length == 0){
		return node;
	}
	else{
		listy = [];
		for(var y in list){
			listy.push("'"+list[y]+"'");
		}
		return listy.toString();
	}
};

/**
* plugNodes() function
* @arg The node to be considered.
* @return a list of nodes adjacent to this node.
**/	
function plugNodes(node){
	var graph = getNodeList();
	node = node.replace(/\D/g,'');
	graph = Object.keys(graph[node]);
	adjecent = [];
	for(var i in graph){
		adjecent.push("'Node "+graph[i]+"'");
	}
	return adjecent.toString();
};

/**
* nextEdge() function
* Considers the values on the edges of a list and returns the edge
* with the highest or lowest value.
* @args op: highest or lowest, field: the value of the edges to be
* checked, list: the list of edges to be checked.
* @return The edge with the highest or lowest value.
**/	
function nextEdge(op, field, list){
	if(typeof(list)=="string"){
		if(list[0] == "["){list = list.substring(1,list.length-1);}
		list = list.split(",");
	}
	if(list[0].search("Node") != -1){
		var id = list[0].substring(6,list[0].length-1);
		var high = nodes.values[id];
		var low = nodes.values[id];
		var type = "Node ";
	}
	else{
		var id = list[0].substring(6,list[0].length-1);
		var high = edges.values[id];
		var low = edges.values[id];
		var type = "Edge ";		
	}
	for(var x in list){
		if(list[x].search("Node") != -1){
			var id = list[x].substring(6,list[x].length-1);
			if(parseInt(nodes.values[id][field]) > parseInt(high[field])){high = nodes.values[id]};
			if(parseInt(nodes.values[id][field]) < parseInt(low[field])){low = nodes.values[id]};
				type = "Node ";
			}
			else{
				var id = list[x].substring(6,list[x].length-1);
				if(parseInt(edges.values[id][field]) > parseInt(high[field])){high = edges.values[id]};
				if(parseInt(edges.values[id][field]) < parseInt(low[field])){low = edges.values[id]};
				type = "Edge ";
			}
	}
	if( op == "Highest"){
		result = high;
	}
	else{
		result = low;
	}
		return type + result.id;
}
	
/**
* isNode() function
* Checks if a variable is a node.
* @arg The variable to be checked.
* @return Boolean whether this is a node
* or not.
**/
function isNode(node){
	if(node.search("Node") != -1){
		return true;
	}
	else{
		return false;
	}
}

/**
* nextOnEdge() function
* Used to find the adjacent node on an edge
* for progression through a graph.
* @args node: The starting node, edge: the edge
* to move across.
* @return The adjacent node.
**/
function nextOnEdge(node,edge){
	edge = edge.replace('[','');
	node = node.substring(6,node.length-1);
	edge = edge.substring(6,edge.length-1);
	if(edges._data[edge].to == node){ return "Node "+edges._data[edge].from;}
	else if(edges._data[edge].from == node){ return "Node "+edges._data[edge].to;}
	else{return null;}
}

/**
* isIn() function 
* Checks a list of an occurance of an item.
* @args item: the item to look for, list: the
* list to check.
* @return a boolean confirming or denying
* the item's presence.
**/	
function isIn(item,list){
	for(var i in list){
		if(list[i] == item){
			return true;
		}
	}
	return false;
}

/**
* listDifference() function
* @args two lists.
* @return the items in the first list
* that are not in the second list.
**/
function listDifference(one,two){
	match = false;
	result=[];
	one = one.split(",");
	two = two.split(",");
	for(var i in one){
		if(one[i][0] == "'"||one[i][0] == '"'){one[i] = one[i].substring(1,one[i].length-1);}
		match = false;
		for(var j in two){
			if(two[j][0] == "'"||two[j][0] == '"'){two[j] = two[j].substring(1,two[j].length-1);}
			if(one[i] == two[j]){
				match = true;
			}	
		}
		if(match == false){
			result.push("'"+one[i]+"'");
		}
	}
	return result.toString();
}

/**
* addToList() function
* Adds an item to a list.
* @args item: the item to be added, list: the
* list the item should be added to.
**/	
function addToList(item,list){
	newList = [];
	if(list.length > 0){
		list = list.split(",");
		for(var i in list){
			if(list[i][0] == "'"){list[i] = list[i].substring(1,list[i].length-1);}
			newList.push("'"+list[i]+"'");
		}
	}
	if(item[0] != "'"){item = "'"+item+"'";}
	newList.push(item);
	return newList.toString();
}

/**
* nonEmpty() function
* @arg the list to check.
* @return boolen whether or not the list is empty.
**/	
function nonEmpty(value){
	return value.length !=0;
}

/**
* changeLabel() function
* Changes the lable if a graph node.
* @args node: the node to be changed, label:
* the text to be made a label.
**/	
function changeLabel(node,label){
	id = node.substring(6,node.length-1);
	nodes.update({ id: id, label: label});
}

/**
* highlightPlug() function
* Considers a variable and calls a function
* to highlight the graph item.
* @arg the variabel to be considered.
**/	
function highlightPlug(id){
	if(id.search("Node") != -1){
		id = id.substring(6,id.length-1);
		highlightNode(id);
	}
	else{
		id = id.substring(6,id.length-1);
		highlightEdge(id);
	}	
}

/**
* colourNumber() function
* colours a node	based on a passed number.
* @args id: the variable to be coloured, colourNumber: the
* number used to generate a colour.
**/
function colourNumber(id,colourPassed){
	num = String(colourPassed);
	num = parseInt(num[0]);
	colour = "";
	switch(num){
		case 0 : colour =  '#cccccc'; break;
		case 1 : colour =  '#ff6666'; break;
		case 2 : colour =  '#ff9966'; break;
		case 3 : colour =  '#ffff66'; break;
		case 4 : colour =  '#ffcc66'; break;
		case 5 : colour =  '#66ff99'; break;
		case 6 : colour =  '#33ffff'; break;
		case 7 : colour =  '#9999ff'; break;
		case 8 : colour =  '#ff99ff'; break;
		case 9 : colour =  '#ffffff'; break;
	}
	varColour(id,colour);
}

/**
* reverseList() function
* Reverses a list.
* @arg the list to be reversed.
* @return the reversed list.
**/
function reverseList(oldList){
	oldList = oldList.split(",");
	var newList = [];
	listLen = oldList.length;
	listLen--;
	while(listLen >= 0){
		newList.push(oldList[listLen]);
		listLen--;
	}
	return newList.toString();
}

/**
* getNodes() function
* Makes a list of all the nodes on the graph.
* @return the list of graph nodes.
**/	
function getNodes(){
	node = Object.keys(nodes._data);
	graph = [];
	for(var i in node){
		graph.push("'Node "+node[i]+"'");
	}
	return graph.toString();
}

/**
* getContrast() function
* Generates a colour contrasting with a given colour.
* @arg A colour to find a contrast.
* @return A contrasting colour.
**/	
function getContrast(hexcolor){
	hexcolor = hexcolor.substring(1,hexcolor.length);
	var colInt = parseInt(hexcolor, 16);
	if(colInt > 8388607.5){
		return '#000000';
	}
	else{
		return'#ffffff';
	}
}

/**
* getDegree() function
* @arg a node to find the degree of
* @return the degree of this node.
**/
function getDegree(node){
	node = parseInt(node.replace( /^\D+/g, ''));
	var graph = getNodeList();
	graph = Object.keys(graph[node]);
	return graph.length;
}