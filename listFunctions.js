/**
 * @license
 * Algorithm visualizer
 *
 * Copyright 2016 University of Leeds.
 * https://www.leeds.ac.uk/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/
 
 /**
 * @fileoverview Functions that control the list visualization
 * in the algorithm visualizer.
 * @authors S.S.Wilson@leeds.ac.uk (Sam Wilson)
 *				  lukeadamroberts@gmail.com (Luke Roberts)
 **/

/**
* listPopOut() function
* Moves the vital variables to the popped-outerHTML
* visualiser.
**/
function listPopOut(){
	popList.listBox.dom.listVisTwo = new ListVisualiser("listBin");
	popList.listBox.dom.listVisTwo.lists = listVis.lists;
	popList.listBox.vis = popList.listBox.dom.listVisTwo;
}

/**
* listPopIn() function
* Moves the vital variables back into the main
* visualiser.
**/
function listPopIn(){
	try{
	listVis.lists = popList.listBox.dom.listVisTwo.lists;
	}
	catch(err){}
	popList.listBox.vis = listVis;
}

/**
* ListVisualiser() function
* Creates a new Instance of the list visualiser.
**/
function ListVisualiser(divName){
  this.divName = divName;
  this.lists = {};
}

ListVisualiser.prototype.setDivName = function(name){
	this.divName = name;
}

/*  *
 * killList() function
 * Removes the list visualiser and
 * resets the data for the lists.
 *  */
function killList(){
	var win = popList.listBox.dom.document;
	 var list = win.getElementById("listBin");
	 while(list.hasChildNodes()){
		 list.removeChild(list.firstChild);
	 }
}

ListVisualiser.prototype.addList = function(name, lst){
  if (name in this.lists){
    console.log("The id: " + name+ " is in use");
    return;
  }
    this.lists[name] = {name: name, index: 0, history: []};
    var c_lst = __copy(lst);
    this.lists[name].history[this.lists[name].index]= c_lst;
    $("."+this.divName,popList.listBox.dom.document).append(__createPanel(name,lst));
    __doList(name,this.lists[name].index,lst);
}

ListVisualiser.prototype.updateList = function(name, lst){
    if (!(name in this.lists)){
      console.log("There is no list with id: " + name + " to update");
      return;
    }
    var c_lst = __copy(lst);
    if(__arraysEqual(c_lst,this.lists[name].history[this.lists[name].index])){
      return;
    }
    this.lists[name].index++;
    this.lists[name].history[this.lists[name].index]= c_lst;
    this.renderUpdate(name);
}

ListVisualiser.prototype.stepEvent = function(){
  // Go and get all the values from the interpretter
  // The list of lists the visualiser knows about are
  // the keys to the dictionary lists
  // Use the updateList function to record the change
  // it is safe just to update every list as it will recognise
  // if it actually changed from the last time
}

ListVisualiser.prototype.renderUpdate = function(name){
  var old = $("#"+name,popList.listBox.dom.document).children()[0];
  old.remove();
  $("#history_"+name,popList.listBox.dom.document).prepend(old);
  __doList(name,this.lists[name].index,this.lists[name].history[this.lists[name].index]);
  if(this.lists[name].index>0){
    __doListDifferent(this,name,this.lists[name].history[this.lists[name].index-1], this.lists[name].history[this.lists[name].index]);
  }
}

ListVisualiser.prototype.__getHistoryIndex = function(name){
  return this.lists[name].index
}

ListVisualiser.prototype.highlight = function(name,history_index,cell){
  $("#"+name+"_"+cell+"_" + history_index,popList.listBox.dom.document).animate({backgroundColor: '#FF0000'}, 'slow');
}

ListVisualiser.prototype.restoreAllLists = function(name,history_index,cell){
  $("#"+name+"_"+cell+"_" + history_index,popList.listBox.dom.document).animate({backgroundColor: '#FF0000'}, 'slow');
}

function __doListDifferent(obj,id,old_lst, lst){
  var result = diff(old_lst,lst);
  if(result.length == 0){
    return
  }
  sofar = -1;
  for( i in result){
    if(result[i][0]== '+' || result[i][0] == '='){
      sofar++;
    }
    if(result[i][0]=='+'){
    $("#"+id+"_"+sofar+"_"+obj.__getHistoryIndex(id),popList.listBox.dom.document).animate({backgroundColor: '#FF0000'}, 'slow');
  }
  }
}

function __arraysEqual(a, b) {
  if (a === b) return true;
  if (a == null || b == null) return false;
  if (a.length != b.length) return false;

  for (var i = 0; i < a.length; ++i) {
    if (a[i] !== b[i]) return false;
  }
  return true;
}

function __copy(o) {
   var output, v, key;
   output = Array.isArray(o) ? [] : {};
   for (key in o) {
       v = o[key];
       output[key] = (typeof v === "object") ? __copy(v) : v;
   }
   return output;
}

function __createPanel(name, list){
  var div =
  "<div class=\"panel panel-default\">"+
    "<div class=\"panel-heading\">"+
      "<h4 class=\"panel-title\">"+
        "<a data-toggle=\"collapse\" href=\"#"+name+"\">"+name+"</a>"+
      "</h4>"+
    "</div>"+
    "<div class=\"center panel-collapse collapse in clearfix\"  id=\""+name+"\">"+
        "<div class=\"text-right\"><button type=\"button\" class=\"btn btn-primary\" data-toggle=\"collapse\" data-target=\"#history_"+name+"\">Show History</button></div>"+
        "<div id=\"history_"+name+"\" class=\"collapse\"></div>"+
    "</div>"+
  "</div>";

  return div;
}
function __doList(id,history_index,lst){
  var table = document.createElement("table");
  var tr = document.createElement("tr");
  tr.id=id+"_row";
  table.appendChild(tr);
  $("#"+id,popList.listBox.dom.document).prepend(table);
  for( item in lst){
    var cell = document.createElement("td");
    cell.id = id+"_"+item+"_"+history_index;
    cell.innerHTML = lst[item];
    $("#"+id+"_row",popList.listBox.dom.document).prepend(cell);
    }

}

/*
Below are functions recycled from underscore.js taken and adapted to work
outside of the node.js framework
*/
var indexMap = function(list) {
  var map = {}
  list.forEach(function(each, i) {
    map[each] = map[each] || []
    map[each].push(i)
  })
  return map
}

var longestCommonSubstring = function(seq1, seq2) {
  var result = {startString1:0, startString2:0, length:0}
  var indexMapBefore = indexMap(seq1)
  var previousOverlap = []
  seq2.forEach(function(eachAfter, indexAfter) {
    var overlapLength
    var overlap = []
    var indexesBefore = indexMapBefore[eachAfter] || []
    indexesBefore.forEach(function(indexBefore) {
      overlapLength = ((indexBefore && previousOverlap[indexBefore-1]) || 0) + 1;
      if (overlapLength > result.length) {
        result.length = overlapLength;
        result.startString1 = indexBefore - overlapLength + 1;
        result.startString2 = indexAfter - overlapLength + 1;
      }
      overlap[indexBefore] = overlapLength
    })
    previousOverlap = overlap
  })
  return result
}

var diff = function(before, after) {
  var commonSeq = longestCommonSubstring(before, after)
  var startBefore = commonSeq.startString1
  var startAfter = commonSeq.startString2
  if (commonSeq.length == 0) {
    var result = before.map(function(each,rank) { return ['-', each,[rank]]})
    return after.map(function(each,rank) { return ['+', each,[rank]]})
  }
  var beforeLeft = before.slice(0, startBefore)
  var afterLeft = after.slice(0, startAfter)
  var equal = after.slice(startAfter, startAfter + commonSeq.length)
    .map(function(each,rank) {return ['=', each,[rank]]})
  var beforeRight = before.slice(startBefore + commonSeq.length)
  var afterRight = after.slice(startAfter + commonSeq.length)
  return union(diff(beforeLeft, afterLeft), equal, diff(beforeRight, afterRight))
}

var orderedSetDiff = function(before, after) {
  var diffRes = diff(before, after)
  var result = []
  diffRes.forEach(function(each) {
    switch(each[0]) {
      case '=':
        result.push(each)
        break
      case '-':
        result.push((after.indexOf(each[1]) > -1) ? ['x', each[1]] : ['-', each[1]])
        break
      case '+':
        result.push((before.indexOf(each[1]) > -1) ? ['p', each[1]] : ['+', each[1]])
    }
  })
  return result
}

var compress = function(diff) {
  var result = []
  var modifier
  var section = []
  diff.forEach(function(each) {
    tt = each.split(",");
    if(modifier && (tt[0] == modifier)) {
      section.push(tt[1])
    } else {
      if(modifier) result.push([modifier, section])
      section = [tt[1]]
      modifier = tt[0]
    }
  })
  if(modifier) result.push([modifier, section])
  return result
}

function union2(array1, array2) {
        var hash = {}, union = [];
        $.each($.merge($.merge([], array1), array2), function (index, value) { hash[value] = value; });
        $.each(hash, function (key, value) { union.push(key); } );
        return union;
    }


function union(array1, array2, array3){
    return union2(union2(array1,array2),array3);
}