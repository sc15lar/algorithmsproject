/**
 * @license
 * Algorithm visualizer
 *
 * Copyright 2016 University of Leeds.
 * https://www.leeds.ac.uk/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

 /**
 * @fileoverview Functions that control the main page of
 * the algorithm visualizer.
 * @author lukeadamroberts@gmail.com (Luke Roberts)
 **/

/**
*  Colours for the different block catagories
**/
var controlCol = 120;
var logicCol = 210;
var mathsCol = 230;
var textCol = 160;
var varCol = 330;
var funcCol = 290;
var listCol = 260;
var objCol = 20;
var graphCol = 180;

/**
* Variable for toggleing the colour switch
**/
var highContrast = false;

/**
* switchColour() function
* Checks whether or not high contrast
* colouring is on and enables / disables
* it accordingly
**/
function switchColour(){
	var placedBlocks = Object.keys(workspace.blockDB_);
	if(highContrast == false){
		Blockly.HSV_VALUE = 0;
		Blockly.HSV_SATURATION = 0;
		for(var i in placedBlocks){
			workspace.blockDB_[placedBlocks[i]].setColour(0);
		}
		highContrast = true;
	}
	else{
		Blockly.HSV_VALUE = 0.65;
		Blockly.HSV_SATURATION = 0.45;
		for(var i in placedBlocks){
			workspace.blockDB_[placedBlocks[i]].setColour(colBank[workspace.blockDB_[placedBlocks[i]].type]);
		}
		highContrast = false;
	}
}

/**
* colBank - a record of each block type's default colour.
* Since the block constructor and superclass are private, this
* is needed to restore colour to on screen blocks after a colour
* change.
**/
var colBank = {"controls_if" : controlCol,"controls_whileUntil": controlCol,"controls_for": controlCol,
					"graph_list_loop": controlCol,"return": controlCol,"logic_compare": logicCol,"logic_operation": logicCol,
					"logic_negate": logicCol,"logic_boolean": logicCol,"logic_null": logicCol,"variables_swap": logicCol,
					"graph_isNode": logicCol,"math_number": mathsCol,"math_arithmetic": mathsCol,"math_single": mathsCol,
					"math_number_property": mathsCol,"math_round": mathsCol,"math_random_int": mathsCol,"math_trig": mathsCol,
					"text": textCol,"text_print": textCol,"text_join": textCol,"text_length": textCol,"text_prompt_ext": textCol,
					"comment": textCol,'variables_get': varCol,'variables_set': varCol,'procedures_defnoreturn': funcCol,
					'procedures_defreturn': funcCol,'procedures_mutatorcontainer': funcCol,'procedures_mutatorarg': funcCol,
					'procedures_callnoreturn': funcCol,'procedures_callreturn': funcCol,'procedures_ifreturn': funcCol,"lists_create_empty": listCol,
					"lists_create_with": listCol,"lists_difference": listCol,"lists_reverse_list": listCol,"lists_is_in": listCol,"lists_merge": listCol,"lists_length": listCol,
					"lists_isEmpty": listCol,"lists_getIndex": listCol,"lists_killIndex": listCol,"lists_setIndex": listCol,"lists_getSublist": listCol,
					"lists_sort": listCol,"lists_split": listCol,"new_object": objCol,"object_access": objCol,"object_value_get": objCol,
					"graph_add_node": graphCol,"edge_add_plug": graphCol,"edge_remove_plug": graphCol,"node_remove_plug": graphCol,
					"set_value_plug": graphCol,"set_value_plug_edge": graphCol,"graph_change_label":graphCol,"graph_node_plug": graphCol,
					"graph_edge_plug": graphCol,"graph_node_degree": graphCol,"graph_plug_nodes": graphCol,"graph_plug_edges": graphCol,
					"graph_list": graphCol,"graph_edge_list": graphCol,"graph_value_plug": graphCol,"graph_value_plug_edge": graphCol,
					"graph_opposite_node": graphCol,"graph_choose_next": graphCol,"graph_highlight_plug": graphCol,"graph_colour_var": graphCol,
					"graph_colour_number": graphCol};

/**
* varDrop() function
* creates other options in the 'table' drop-down
* menu that shows the different variables.
**/
function varDrop(){
	var menu = document.getElementById('tableDropDown');
	killVarDrop();
	var keys = Object.keys(varVal);
	var varNum = keys.length;
	var varHeadItem =  document.createElement("li");
	varHeadItem.setAttribute("id","varHead");
	varHeadItem.innerHTML ="Hide variables";
	menu.appendChild(varHeadItem);
	for(var i in keys){
		var newItem =  document.createElement("li");
		var varCheck = document.createElement("input");
		varCheck.setAttribute("type","checkbox");
		varCheck.setAttribute("value", keys[i]);
		varCheck.setAttribute("onchange", "hideColumn(value)");
		varCheck.setAttribute("id", "varCheck");
		newItem.innerHTML = keys[i]+"       ";
		newItem.setAttribute("id","varMenu");
		newItem.appendChild(varCheck);
		menu.appendChild(newItem);
	}
}

/**
* killVarDrop() function
* removes the varDrop options from the menu
**/
function killVarDrop(){
	var box = document.getElementById('tableDropDown');
	while($("#tableDropDown").children().length > 1){
		box.removeChild(box.lastChild);
	}
}

/**
* hideColumn() function
* Hides a column of the trace table.
* @arg the variable name of the table it represents.
**/
function hideColumn(id){
	var head = document.getElementById("headerTable");
	var varIndex = null;
	for(var i = 0; i < head.rows[0].cells.length; i++){
		if(head.rows[0].cells[i].innerHTML == id){
			varIndex = i;
		}
	}
	$('#headerTable tr > *:nth-child('+(varIndex+1)+')').toggle();
	$('#bodyTable tr > *:nth-child('+(varIndex+1)+')').toggle();
}

/**
* Event listeners checking for key presses
**/
$("#userInput").on('keydown', function(e){
	keyTest(e);
});

$(document).on('keydown', function(e){
if(e.which == 75 && $('#userInput').is(':focus') == false){
	if(isRunning == true){
		stopRun();
	}
	else{
		runCode();
	}
}
});

/**
* Event listener checking for changes in the userString var-->
**/
Object.defineProperty(window, 'userString',{
	get:function(){
		return currString;
	},
	set:function(value){
		currString = value;
		if(waiting == true){
			printToConsole("AV> "+value);
			waiting = false;
			lock = false;
			if(msgType == "NUMBER"){
				var ob = myInterpreter.createPrimitive(parseInt(value));
			}
			else{
				var ob = myInterpreter.createPrimitive(value);
			}
			myInterpreter.setValueToScope(changeVar, ob);
			varVal = updateVarVal();

			if(isRunning == true){
				running(myInterpreter);
			}
		}
		else{
			consoleEx();
		}
		box = document.getElementById("console");
		box.scrollTop = box.scrollHeight;
	}
});

/**
* resetLink() function
* function for resetting the onClick of a button
* @arg the button to be reset.
**/
function resetLink(item){
	var button = document.getElementById(popList[item].key+'Button');
	button.onclick = function onclick(event){changeTab('#'+item);};
}

/**
* changeTab() function
* Function for changing the bootstrap tab
* @arg the tab to be changed too.
**/
function changeTab(href) {
	var href = href;
	$( '[data-toggle="tab"][href="' + href + '"]' ).trigger( 'click' );
	table = $("#tableNav").attr('class');
	graph = $("#graphNav").attr('class');
	list = $("#listNav").attr('class');
	if(href == "#graphBox"){
		$('#tableNav').removeClass('active');
		$('#dropTab').removeClass('active');
		$('#listNav').removeClass('active');
		$('#graphNav').addClass('active');
		$('#dropGraph').addClass('active');
	}
	else if(href == "#tableBox"){
		$('#graphNav').removeClass('active');
		$('#dropGraph').removeClass('active');
		$('#listNav').removeClass('active');
		$('#tableNav').addClass('active');
		$('#dropTab').addClass('active');
	}
	if(href == "#listBox"){
		$('#tableNav').removeClass('active');
		$('#dropTab').removeClass('active');
		$('#listNav').addClass('active');
		$('#graphNav').removeClass('active');
		$('#dropGraph').removeClass('active');
	}
}

/**
* Event listener for recentering the graph when changing tabs
**/
$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
network.fit();
});

/**
* The global variables used to operate the code.
**/
<!--The list of variables and their values-->
var varVal = {};
var holder = {};
var network = null;
var nodes = undefined;
var edges = undefined;
var data = undefined;
var backup = {};
var seed = 2;
var curHigh = undefined;
var lastHigh = {id: -1};
var iteration = 0;
var curStep;
var curID;
var lastChange = 'null';
var stopEx = false;
var isParsed = false;
var currString = '';
var waiting = false;
var lock = false;
var changeVar;
var msgType;
var isRunning = false;
<!--Variables for the list visualizer-->
var lists = {};
var listVis = new ListVisualiser("listBin");
<!--The pop out directory-->
var popList = {
	tableBox:{popped:false, dom: window, key: 'table'},
	graphBox:{popped:false, dom: window,key: 'graph'},
	console:{popped:false, dom: window},
	listBox:{popped:false, dom: window, vis: listVis, key: 'list'}
};


/**
*Inject Blockly into the div created above
**/
var workspace = Blockly.inject('blocklyDiv',
{toolbox: document.getElementById('toolbox'), sounds:false, comments: false});
Blockly.Xml.domToWorkspace(document.getElementById('startBlocks'),
				   workspace);
var myInterpreter = null;
/**
* initApi() function
* creates an instance of the interpreter and
* specifies all the wrappers used to call custom functions
* from the code.
**/
function initApi(interpreter, scope) {
try{
Blockly.JavaScript.definitions_ = {};
}
catch(err){alert(err)};
// Add an API function for the alert() block.
var wrapper = function(text) {
	text = text ? text.toString() : '';
	return interpreter.createPrimitive(alert(text));
};
interpreter.setProperty(scope, 'alert',
interpreter.createNativeFunction(wrapper));

// Add an API function for the change() block.
var wrapper = function(text) {
	text = text ? text.toString() : '';
	return interpreter.createPrimitive(change(text));
};
interpreter.setProperty(scope, 'change',
interpreter.createNativeFunction(wrapper));

// Add an API function for the highlightNode() block.
var wrapper = function(text) {
	text = text ? text.toString() : '';
	return interpreter.createPrimitive(highlightNode(text));
};
interpreter.setProperty(scope, 'highlightNode',
interpreter.createNativeFunction(wrapper));

// Add an API function for the trueSplit() block.
var wrapper = function() {
	return interpreter.createPrimitive(String.prototype.trueSplit());
};
interpreter.setProperty(scope, 'trueSplit',
interpreter.createNativeFunction(wrapper));

// Add an API function for the getDegree() block.
var wrapper = function(text) {
	text = text ? text.toString() : '';
	return interpreter.createPrimitive(getDegree(text));
};
interpreter.setProperty(scope, 'getDegree',
interpreter.createNativeFunction(wrapper));

// Add an API function for the highlightEdge() block.
var wrapper = function(text) {
	text = text ? text.toString() : '';
	return interpreter.createPrimitive(highlightEdge(text));
};
interpreter.setProperty(scope, 'highlightEdge',
interpreter.createNativeFunction(wrapper));

// Add an API function for the highlightPlug() block.
var wrapper = function(text) {
	text = text ? text.toString() : '';
	return interpreter.createPrimitive(highlightPlug(text));
};
interpreter.setProperty(scope, 'highlightPlug',
interpreter.createNativeFunction(wrapper));

// Add an API function for the getNodes() block.
var wrapper = function() {
	return interpreter.createPrimitive(getNodes());
};
interpreter.setProperty(scope, 'getNodes',
interpreter.createNativeFunction(wrapper));

// Add an API function for the addNode() block.
var wrapper = function(text) {
	text = text ? text.toString() : '';
	return interpreter.createPrimitive(addNode(text));
};
interpreter.setProperty(scope, 'addNode',
interpreter.createNativeFunction(wrapper));

// Add an API function for the removeNode() block.
var wrapper = function(text) {
	text = text ? text.toString() : '';
	return interpreter.createPrimitive(removeNode(text));
};
interpreter.setProperty(scope, 'removeNode',
interpreter.createNativeFunction(wrapper));

// Add an API function for the addEdge() block.
 var wrapper = function(from,to) {
	from = from ? from.toString() : '';
	to = to ? to.toString() : '';
	return interpreter.createPrimitive(addEdge(from,to));
};
interpreter.setProperty(scope, 'addEdge',
interpreter.createNativeFunction(wrapper));

// Add an API function for the removeEdge() block.
var wrapper = function(text) {
	text = text ? text.toString() : '';
	return interpreter.createPrimitive(removeEdge(text));
};
interpreter.setProperty(scope, 'removeEdge',
interpreter.createNativeFunction(wrapper));

// Add an API function for the iterate() block.
var wrapper = function(text) {
	text = text ? text.toString() : '';
	return interpreter.createPrimitive(iterate(text));
};
interpreter.setProperty(scope, 'iterate',
interpreter.createNativeFunction(wrapper));

// Add an api function for the nodeColour() block
var wrapper = function(id, color) {
	id = id ? id.toString() : '';
	color = color ? color.toString() : '';
	return interpreter.createPrimitive(nodeColour(id,color));
};
interpreter.setProperty(scope, 'nodeColour',
interpreter.createNativeFunction(wrapper));

// Add an api function for the changeLabel() block
var wrapper = function(node, label) {
	node = node ? node.toString() : '';
	label = label ? label.toString() : '';
	return interpreter.createPrimitive(changeLabel(node,label));
};
interpreter.setProperty(scope, 'changeLabel',
interpreter.createNativeFunction(wrapper));

//Add an API function for the edgeColour() block
var wrapper = function(id, color) {
	id = id ? id.toString() : '';
	color = color ? color.toString() : '';
	return interpreter.createPrimitive(edgeColour(id,color));
};
interpreter.setProperty(scope, 'edgeColour',
interpreter.createNativeFunction(wrapper));

//Add an API function for the setValue() block
var wrapper = function(id, field,value) {
	id = id ? id.toString() : '';
	field = field ? field.toString() : '';
	value = value ? value.toString() : '';
	return interpreter.createPrimitive(setValue(id,field,value));
};
interpreter.setProperty(scope, 'setValue',
interpreter.createNativeFunction(wrapper));

//Add an API function for the setNodeValue() block
var wrapper = function(id, field,value) {
	id = id ? id.toString() : '';
	field = field ? field.toString() : '';
	value = value ? value.toString() : '';
	return interpreter.createPrimitive(setNodeValue(id,field,value));
};
interpreter.setProperty(scope, 'setNodeValue',
interpreter.createNativeFunction(wrapper));

//Add an API function for the getNodeValue() block
var wrapper = function(id, field) {
	id = id ? id.toString() : '';
	field = field ? field.toString() : '';
	return interpreter.createPrimitive(getNodeValue(id,field));
};
interpreter.setProperty(scope, 'getNodeValue',
interpreter.createNativeFunction(wrapper));

//Add an API function for the getNodeValue() block
var wrapper = function(id, field) {
	id = id ? id.toString() : '';
	field = field ? field.toString() : '';
	return interpreter.createPrimitive(getValue(id,field));
};
interpreter.setProperty(scope, 'getValue',
interpreter.createNativeFunction(wrapper));

//Add an API function for the setEdgeValue() block
var wrapper = function(id, field,value) {
	id = id ? id.toString() : '';
	field = field ? field.toString() : '';
	value = value ? value.toString() : '';
	return interpreter.createPrimitive(setEdgeValue(id,field,value));
};
interpreter.setProperty(scope, 'setEdgeValue',
interpreter.createNativeFunction(wrapper));

//Add an API function for the getEdgeValue() block
var wrapper = function(id, field) {
	id = id ? id.toString() : '';
	field = field ? field.toString() : '';
	return interpreter.createPrimitive(getEdgeValue(id,field));
};
interpreter.setProperty(scope, 'getEdgeValue',
interpreter.createNativeFunction(wrapper));

//Add an API function for the varColour() block
var wrapper = function(id, colour) {
	id = id ? id.toString() : '';
	colour = colour ? colour.toString() : '';
	return interpreter.createPrimitive(varColour(id,colour));
};
interpreter.setProperty(scope, 'varColour',
interpreter.createNativeFunction(wrapper));

//Add an API function for the colourNumber() block
var wrapper = function(id, colour) {
	id = id ? id.toString() : '';
	colour = colour ? colour.toString() : '';
	return interpreter.createPrimitive(colourNumber(id,colour));
};
interpreter.setProperty(scope, 'colourNumber',
interpreter.createNativeFunction(wrapper));

//Add an API function for the varColour() block
var wrapper = function(list) {
	list = list ? list.toString() : '';
	return interpreter.createPrimitive(iterateThroughList(list));
};
interpreter.setProperty(scope, 'iterateThroughList',
interpreter.createNativeFunction(wrapper));

// Add an API function for the prompt() block.
var wrapper = function(text) {
	text = text ? text.toString() : '';
	return interpreter.createPrimitive(prompt(text));
};
interpreter.setProperty(scope, 'prompt',
interpreter.createNativeFunction(wrapper));

// Add an API function for the nonEmpty() function.
var wrapper = function(text) {
	text = text ? text.toString() : '';
	return interpreter.createPrimitive(nonEmpty(text));
};
interpreter.setProperty(scope, 'nonEmpty',
interpreter.createNativeFunction(wrapper));

// Add an API function for the plugNodes() block.
  var wrapper = function(text) {
	text = text ? text.toString() : '';
	return interpreter.createPrimitive(plugNodes(text));
};
interpreter.setProperty(scope, 'plugNodes',
interpreter.createNativeFunction(wrapper))

// Add an API function for the plugEdge() block.
var wrapper = function(text) {
	text = text ? text.toString() : '';
	return interpreter.createPrimitive(plugEdges(text));
};
interpreter.setProperty(scope, 'plugEdges',
interpreter.createNativeFunction(wrapper));

// Add an API function for the nextEdge() block.
var wrapper = function(op,field,list) {
	op = op ? op.toString() : '';
	field = field ? field.toString() : '';
	list = list ? list.toString() : '';
	return interpreter.createPrimitive(nextEdge(op,field,list));
};
interpreter.setProperty(scope, 'nextEdge',
interpreter.createNativeFunction(wrapper));

// Add an API function for the nextOnEdge() block.
var wrapper = function(node,edge) {
	node = node ? node.toString() : '';
	edge = edge ? edge.toString() : '';
	return interpreter.createPrimitive(nextOnEdge(node,edge));
};
interpreter.setProperty(scope, 'nextOnEdge',
interpreter.createNativeFunction(wrapper));

// Add an API function for the isIn() block.
var wrapper = function(item,list) {
	item = item ? item.toString() : '';
	list = list ? list.toString() : '';
	return interpreter.createPrimitive(isIn(item,list));
};
interpreter.setProperty(scope, 'isIn',
interpreter.createNativeFunction(wrapper));

// Add an API function for the addToList() block.
var wrapper = function(item,list) {
	item = item ? item.toString() : '';
	list = list ? list.toString() : '';
	return interpreter.createPrimitive(addToList(item,list));
};
interpreter.setProperty(scope, 'addToList',
interpreter.createNativeFunction(wrapper));

// Add an API function for the listDifference() block.
var wrapper = function(one,two) {
	one = one ? one.toString() : '';
	two = two ? two.toString() : '';
	return interpreter.createPrimitive(listDifference(one,two));
};
interpreter.setProperty(scope, 'listDifference',
interpreter.createNativeFunction(wrapper));

// Add an API function for the reverseList() block.
var wrapper = function(text) {
	text = text ? text.toString() : '';
	return interpreter.createPrimitive(reverseList(text));
};
interpreter.setProperty(scope, 'reverseList',
interpreter.createNativeFunction(wrapper));

// Add an API function for the plugEdge() block.
var wrapper = function(text) {
	text = text ? text.toString() : '';
	return interpreter.createPrimitive(isNode(text));
};
interpreter.setProperty(scope, 'isNode',
interpreter.createNativeFunction(wrapper));

// Add an API function for the printToConsole() block.
var wrapper = function(text) {
	text = text ? text.toString() : '';
	return interpreter.createPrimitive(printToConsole(text));
};
interpreter.setProperty(scope, 'printToConsole',
interpreter.createNativeFunction(wrapper));


// Add an api function for the getFromConsole() block
var wrapper = function(msg,type) {
	msg = msg ? msg.toString() : '';
	type = type ? type.toString() : '';
	return interpreter.createPrimitive(getFromConsole(msg,type));
};
interpreter.setProperty(scope, 'getFromConsole',
interpreter.createNativeFunction(wrapper));

// Add an API function for highlighting blocks.
var wrapper = function(id) {
	id = id ? id.toString() : '';
	curID = id;
	//Get the id of the current block
	bloc = workspace.getBlockById(id);
	//Get the code for this block and remove all non-relavant lines
	curStep = Blockly.JavaScript.blockToCode(bloc)
	curStep= curStep.substring(curStep.search('\n')+1,curStep.length);
	curStep= curStep.substring(0,curStep.search('\n'));
	return interpreter.createPrimitive(highlightBlock(id));
};
interpreter.setProperty(scope, 'highlightBlock',
interpreter.createNativeFunction(wrapper));
}
var highlightPause = false;

/**
* highlightBlock() function
* injects code to highlight the blocks
* when that block's code is being executed.
* @arg the id of the block.
**/
function highlightBlock(id) {
  workspace.highlightBlock(id);
  highlightPause = true;
}

/**
* running() function
* replaces the run() function of the interpreter, uses
* a numerical value given by the input range slider to
* control the speed of execution. Also allows for pausing
* by checking for a push of the pause button with every cycle.
* @arg the interpreter to run.
* @return if the program is paused, return the interpreter.
**/
function running(ob){
	isParsed = false;
	if(!ob.paused_){
		if(afterStep(ob)){
			var inc = document.getElementById("runSlider").value;
			inc = inc *-1;
			if(stopEx == false){
			setTimeout(function(){running(ob);}, inc);
			}
			else{
				isParsed = true;
				stopEx = false;
			}
		}
	}
	return ob.paused_;
}

/**
* parseCode() function
* Prepare the code for stepping
**/
function parseCode() {
  Blockly.JavaScript.STATEMENT_PREFIX = 'highlightBlock(%1);\n';
  Blockly.JavaScript.addReservedWords('highlightBlock');
  var code = Blockly.JavaScript.workspaceToCode(workspace);
  myInterpreter = new Interpreter(code, initApi);
  document.getElementById('stepButton').disabled = '';
  document.getElementById('runButton').disabled = '';
  highlightPause = false;
  workspace.traceOn(true);
  workspace.highlightBlock(null);
  varVal = updateVarVal();
  holder = varVal;
  curStep = null;
  if(nodes.values == undefined){
	  nodes.values = {};
	  for(var z in Object.keys(nodes._data)){
		var curNode = Object.keys(nodes._data)[z];
		nodes.values[nodes._data[z].id]={id: curNode};
		nodes.update({id:nodes._data[z].id, title : "No values"});
	  }
  }
  if(edges.values == undefined){
	  edges.values = {};
	  for(var a in Object.keys(edges._data)){
		var curEdge = Object.keys(edges._data)[a];
		edges.values[curEdge]={id: curEdge};
		edges.update({id:curEdge, title : "No values"});
	  }
  }
  resetTable();
  clearGraph();
  curHigh = undefined;
  lastHigh = {id: -1};
  iteration = 0;
  killList();
  lists = {};
  popList.listBox.vis.lists = {};
  varDrop();
}

/**
* getCode() function
* Save the contents of the workspace as python code
**/
function getCode(){
	try{
		var temp = document.createElement("a");
		document.body.appendChild(temp);
		temp.style = "display: none";
		var code = Blockly.Python.workspaceToCode(workspace);
		code = code.replace(/\n/g, "\r\n");
		blob = new Blob([code], {type: "octet/stream"});
		url = URL.createObjectURL(blob);
		temp.href = url;
		temp.download = "blocklyCode.txt";
		temp.click();
		URL.revokeObjectURL(url);
	}
	catch(err){
		alert("Object/Graph blocks cannot be saved as python!");
	}
}

/**
* getVaris() function
* Get the variables out of the scope of the function
* @return the list of variable being used.
**/
function getVaris(){
	var varList = [];
	for(var i in myInterpreter.ast.body){
		if(myInterpreter.ast.body[i].type == "VariableDeclaration"){
			for(var n = 0; n < myInterpreter.ast.body[i].declarations.length; n++){
				varList.push(myInterpreter.ast.body[i].declarations[n].id.name);
			}
		}
	}
	return varList;
}

/**
* updateVarVal() function
* Updates the record of the variables being used in the
* program. This is used to generate the trace table.
**/
function updateVarVal(){
	var varVall = {};
	var varList = getVaris();
	for(var i in varList){
		if(!myInterpreter.getValueFromScope(varList[i]).data){
			if(myInterpreter.getValueFromScope(varList[i]).toString() == 'undefined')
			{
				varVall[varList[i]] = '';
			}
			else{
				if(!myInterpreter.getValueFromScope(varList[i]).length && myInterpreter.getValueFromScope(varList[i]).type == "object"){
					var newOb = {};
					try{
					var itemList = Object.keys(myInterpreter.getValueFromScope(varList[i]).properties);
					for(var j in itemList){
						if(myInterpreter.getValueFromScope(varList[i]).properties[itemList[j]].data == 'undefined'){
							var value = myInterpreter.getValueFromScope(varList[i]).properties[itemList[j]].data;
						}
						else{
							var value = myInterpreter.getValueFromScope(varList[i]).properties[itemList[j]].toString();
						}
						newOb[itemList[j]] = value;
					}
					varVall[varList[i]] = JSON.stringify(newOb);
					}catch(err){}
				}
				else{
					tester = myInterpreter.getValueFromScope(varList[i]).toString();
					if(tester.search('object') != -1){
						var contentList = Object.keys(myInterpreter.getValueFromScope(varList[i]).properties);
						var content = {};
						for(var n in contentList){
							content[contentList[n]] = myInterpreter.getValueFromScope(varList[i]).properties[contentList[n]].data;
						}
						varVall[varList[i]] =JSON.stringify(content);
					}
					else if(myInterpreter.getValueFromScope(varList[i]).type == "object"){
						varVall[varList[i]] = '['+myInterpreter.getValueFromScope(varList[i]).toString()+']';
					}
					else{
						varVall[varList[i]] = myInterpreter.getValueFromScope(varList[i]).toString();
					}
				}
			}
		}
		else{
			varVall[varList[i]] = myInterpreter.getValueFromScope(varList[i]).data;
		}
	}
	var obList = Object.keys(varVal);
	for(var j in obList){
		try{
			if(varVal[obList[j]].charAt(0) == '[')
			{
				var newList = false;
				if(lists[obList[j]] == undefined){
				newList = true;
				}
				lists[obList[j]] = varVal[obList[j]].substring(1,varVal[obList[j]].length-1).split(',');
				if(newList == true){
				popList.listBox.vis.addList(obList[j],lists[obList[j]]);
				}
				else{
				popList.listBox.vis.updateList(obList[j],lists[obList[j]]);
				}

			}
		} catch(err){}
	}
	return varVall;
}

/**
* stepCode() function
* Step through the code
**/
function stepCode() {
if(lock != true){
	if(isParsed == false){
		parseCode();
		isParsed=true;
	}
	  try {
		var ok = afterStep(myInterpreter);
	  }
		catch(err){console.log(err.stack);}
		finally {
		if (!ok) {
		  document.getElementById('stepButton').disabled = 'disabled';
		  isParsed = false;
		  return;
		}
	  }
	  if (highlightPause) {
		highlightPause = false;
	  } else {
			stepCode();
	  }
  }
  else{
	alert("You cannot step until you have responded to the console.");
  }
}

/**
* compare() function
* compares two variables
* @args two lists or objects
* @return true or false depending on
* whether the two variables are different.
**/
function compare(older,newer){
	oldNum = Object.keys(older).length;
	newNum = Object.keys(newer).length;
	if(oldNum != newNum){
		return true;
	}
	for(var i in older){
		if(older[i] != newer[i]){
			return true;
		}
	}
	return false;
}

/**
* afterStep() function
* Wrapper function for the interpreter step function
* checks to see if any variables have changed and
* updates the table accordingly.
* @arg whatever interpreter object is being stepped.
* @return boolean, false to indicate a pause, else
* true.
**/
function afterStep(obj){
	try{
		var result = obj.step();
	}catch(err){alert(err);}
	varVal = updateVarVal();
	if(compare(holder,varVal) == true && curStep != lastChange){
		lastChange = curStep;
		holder = varVal;
		iteration++;
		if(waiting != true){
			stepTable();
			box = document.getElementById("console");
			box.scrollTop = box.scrollHeight;
		}
	}
	return result;
}

/**
* runCode() function
* Executes the running() function.
* Alerts a warning in event of an error.
**/
function runCode() {
	if(lock != true){
		if(isParsed == false){
			parseCode();
			isParsed=true;
		}
		try{
			isRunning = true;
			running(myInterpreter);
		}
		catch(err){
			alert(err);
		}
	}
	else{
		alert("You cannot run until you have responded to the console.");
	}
}

/**
* allStop() function
* Ceases the current execution
**/
function allStop(){
	isRunning = false;
	stopEx = true;
	parseCode();
}

/**
* stopRun() function
* Stops the running() function by changing the
* stopEx variable to false.
**/
function stopRun(){
	isRunning = false;
	stopEx = true;
}

/**
* showCode() function
* Show the javascript of the workspace
**/
function showCode() {
	var code = Blockly.JavaScript.workspaceToCode(workspace);
	alert("The current code looks like this: \n\n" + code);
}

/**
* showVars() function
* A function to show the variables in the workspace
**/
function showVars(){
	var code = Blockly.Variables.allVariables(workspace);
	alert(code);
}

/**
* loadBlocks() function
* Load a text file and implement it in the workspace
**/
function loadBlocks(){
    Blockly.mainWorkspace.clear()
		source = document.getElementById("xmlFile");
		file = source.files[0];
		reader = new FileReader();
		reader.onload = function() {
		var text = reader.result;
		var xml = Blockly.Xml.textToDom(text);
		Blockly.Xml.domToWorkspace(xml, workspace);
	}
	reader.readAsText(file);
}

/**
* saveBlocks() function
* Create a textfile of the workspace then download it
**/
function saveBlocks(){
	var temp = document.createElement("a");
	document.body.appendChild(temp);
	temp.style = "display: none";
	var xml = Blockly.Xml.workspaceToDom(workspace);
	var xml_text = Blockly.Xml.domToText(xml);
	blob = new Blob([xml_text], {type: "octet/stream"});
	url = URL.createObjectURL(blob);
	temp.href = url;
	temp.download = "blocklySave.txt";
	temp.click();
	URL.revokeObjectURL(url);
}

/**
* saveGraph() function
* Create a textfile of the graph and save it to file
**/
function saveGraph(){
	removePrimatives();
	var temp = document.createElement("a");
	document.body.appendChild(temp);
	temp.style = "display: none";
	var graph = JSON.stringify(data);
	blob = new Blob([graph], {type: "octet/stream"});
	url = URL.createObjectURL(blob);
	temp.href = url;
	temp.download = "graphSave.txt";
	temp.click();
	URL.revokeObjectURL(url);
}

/**
* loadGraph() function
* Load a text file and implement it as the graph
**/
function loadGraph(){
		source = document.getElementById("graphFile");
		file = source.files[0];
		reader = new FileReader();
		reader.onload = function() {
		var text = reader.result;
		newGraph=JSON.parse(text);
		for(var i in newGraph.nodes){
			nodes[i] = newGraph.nodes[i];
		}
		for(var y in newGraph.edges){
			edges[y] = newGraph.edges[y];
		}
		network.destroy;
		draw();
		network.fit();
	}
	reader.readAsText(file);
}

/**
* cleanGraph() function
* removes all nodes and edges from the
* graph.
**/
function cleanGraph(){
	nodes._data ={0:{id:"0",label:"0"}};
	edges._data ={};
	network.destroy;
	draw();
	network.fit();
}
