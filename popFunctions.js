/**
 * @license
 * Algorithm visualizer
 *
 * Copyright 2016 University of Leeds.
 * https://www.leeds.ac.uk/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/
 
 /**
 * @fileoverview Functions that control the detachment of 
 * the various visualizers in the algorithm visualizer.
 * @author lukeadamroberts@gmail.com (Luke Roberts)
 **/
 
/**
* popOut() function
* Removes a page element and places it in a seperate browser window.
**/
function popOut(id){
	if($('#inPic').length != 0 && id=="tableBox"){
		parseCode();
	}
	var popObj = popList[id].dom.document.getElementById(id);
	popList[id].dom = window.open("", id, "location=no,width=500,height=500");
	popList[id].dom.document.write('<html><head><meta charset="utf-8"><!--Page title--><title>Algorithm Visualizer</title><!--Icon for the page--><link rel="shortcut icon" href="favicon.ico" /><!--Ensure compatability with mobile devices--><meta name="viewport" content="width=device-width, initial-scale=1"><!-- Bootstrap CSS --><link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"><link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css"><!-- jQuery library --><script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script><!--jQuery UI library--><link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css"><script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script><!-- Bootstrap JavaScript --><script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script><!---Import the blockly resources---><script src="acorn.js"></script><script src="interpreter.js"></script><!--Import the Visja reasources--><script type="text/javascript" src="Vis/exampleUtil.js"></script><script type="text/javascript" src="Vis/vis-network.min.js"></script><script type="text/javascript" src="Vis/vis.js"></script><link href="Vis/vis.css" rel="stylesheet" type="text/css" />		<!--Add JS file for the graph creation functions--><script src="graphFunctions.js"></script>	<script src="googleAnalytics.js"></script><!--Add the JS file for the console functions--><script src="consoleFunctions.js"></script>	<!--Add custom CSS--><link rel="stylesheet" href="style.css" ><!--Add JS file for the table creation functions--><script src="tableFunctions.js"></script><!--Add JS file for the pop-out functions--><script src="popFunctions.js"></script><!--Add JS file for the list functions--><script src="listFunctions.js"></script><script>$(window).resize(function(){try{scaleTab();}catch(err){}});</script></head><body>');
	popList[id].dom.document.write(popObj.outerHTML);
	popList[id].dom.document.write('</body></html>');
	popList[id].dom.document.popList = popList;
	popList[id].popped = true;
	switch(id){
		case 'tableBox' :
					tablePopOut();
					break;
		case 'console' :
					consolePopOut();
					break;
		case 'graphBox' :
					graphPopOut();
					break;	
		case 'listBox':
					listPopOut();
					break;
	}
	popList[id].dom.addEventListener("beforeunload",function(){popIn(id);});
	reArrange();
}

/**
* popIn() function
* Places a detacted element and places it back in the main window.
**/
function popIn(id){
  popList[id].popped = false;
  retObj = window;
  retObj.document.getElementById(id).innerHTML = popList[id].dom.document.getElementById(id).innerHTML;	
  popList[id].dom = retObj;
  switch(id){
	case 'tableBox':
	  	changeTab('#tableBox');
		restoreTab();
		break;
	case 'console':
		consolePopIn();
		break;
	case 'graphBox':
		graphPopIn();
		break;
	case 'listBox':
		listPopIn();
		break;
  }
  reArrange();
  return null;
}

/**
* reArrange() function
* Moves the div elements on the page depending on 
* what elements have been detached.
**/
function reArrange(){
	var tabs = true;
	$('#otherSide').css('height', $("#bodyRow").height()*0.7);
	$('#otherSide').css('display', 'block');
	$('#tableSide').css('display',"block");
	$('#blockSide').css('width', '50%');

	var tabList = Object.keys(popList);
	tabList.splice(tabList.indexOf("console"),1);
	var popped = [];
	var unpopped = [];
	for(var i in tabList){
		if(popList[tabList[i]].popped == true){
			popped.push(tabList[i]);
		}
		else{
			unpopped.push(tabList[i]);
		}
	}
	var active = 'none';
	for(var i in tabList){
		var quest = $('#'+popList[tabList[i]].key+"Nav").hasClass("active");
		if(quest == true){
			active = tabList[i];
		}
	}
	for(var i in popped){
		var button = popList[popped[i]].key+"Button";
		document.getElementById(button).onclick = function onclick(event){changeTab('')};
	}
	for(var i in unpopped){
		var button = popList[unpopped[i]].key+"Button";
		resetLink(unpopped[i]);
	}
	if(unpopped.length == 0){
		$('#otherSide').css('display', 'none');
		tabs = false;
	}
	else{
			changeTab('#'+unpopped[0]);
	}
	if(popList.console.popped == true){
		if(tabs == true){
			$('#otherSide').css('height', $("#bodyRow").height());
			$('#console').css('display',"none" );
		}
		else{
			$('#tableSide').css('display',"none");
			$('#blockSide').css('width', '100%');
		}
	}
	else{
		if(tabs == true){
		$('#console').css('height', $("#bodyRow").height()*0.30);
		$('#console').css('display',"block");
		}
		else{
		$('#console').css('height', $("#bodyRow").height()*1);
		$('#console').css('display',"block");
		}
	}
	window.dispatchEvent(new Event('resize'));
}	