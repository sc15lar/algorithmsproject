/**
 * @license
 * Algorithm visualizer
 *
 * Copyright 2016 University of Leeds.
 * https://www.leeds.ac.uk/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/
 
 /**
 * @fileoverview Functions that control the table visualisation
 * in the algorithm visualizer.
 * @author lukeadamroberts@gmail.com (Luke Roberts)
 **/

/**
* Global variables involving tables
**/			
<!--Toggle for whether or not the table is currently embedded or not-->
var tablePopped = false;
var tabWin;
var fixTab;

$(window).ready(function(){
		if(parent.opener != null){
		alert("This is a child");
		}
});

/**
* tablePopOut()
* handles the restructuring of the table when moving between
* the page and the pop out
**/
function tablePopOut(){
		fixTab = setInterval(function(){scaleTab();},100);
}

/**
* Jquery event listeners to modify the layout of the page.
**/
$(document).ready(function() {
	
  /**
  * setHeight() function
  * Adjusts the height of the page elements to fit the 
  * new screen dimensions.
  **/
  function setHeight() {
	var windowHeight = $(window).innerHeight()*0.98;
	$('#navRow').css('height', $("#navi").height()+2);
	$('#bodyRow').css('height', windowHeight - $("#navRow").height());
	$('#bodyRow').css('top', $("#navRow").height());
	$('.col-md-6').css('height', $("#bodyRow").height());
	$('.col-md-6').css('min-height', '540px');
	$('.blocklyToolboxDiv').css("top",$("#navi").height()+2);

	if((popList.tableBox.popped==false && popList.graphBox.popped==false)){		
		if(popList.console.popped==false){
			if($(window).width() > 992){
				$('#console').css('height', $("#bodyRow").height()*0.30);
				$('#tableBox').css('height', $("#bodyRow").height()*0.70);
				$('#graphBox').css('height', $("#bodyRow").height()*0.70);
				$('#mynetwork').css('height', $("#bodyRow").height()*0.70);
				$('.tab-content').css('height', $("#bodyRow").height()*0.70);
			}
			else{
				$('#console').css('height', $(".col-md-6").height()*0.30);
				$('#tableBox').css('height', $(".col-md-6").height()*0.70);
				$('#graphBox').css('height', $(".col-md-6").height()*0.70);
				$('#mynetwork').css('height', $(".col-md-6").height()*0.70);
				$('.tab-content').css('height', $(".col-md-6").height()*0.70);	
			}
		}
		else{
			$('#tableBox').css('height', $("#bodyRow").height());
			$('#graphBox').css('height', $("#bodyRow").height());
			$('#mynetwork').css('height', $("#bodyRow").height());
			$('.tab-content').css('height', $("#bodyRow").height());
		}
		$('.body-cell.col').css('backgroundColor', 'white');
		$('#tablePopOut').css('display', 'block');
	}
  };
  setHeight();

  $(window).resize(function() {
		setHeight();
		setTableBody();		
  });
  setHeight();
  setTimeout(function(){setHeight()},500);
});

/**
* setTableBody() function
* Adjusts the layout of the table visualisation depending
* on the size of the window.
**/
function setTableBody()
{
	if(popList.tableBox.popped == false){
		$("#bodyBox").height(($("#tableBox").height()) - $(".table-header").height());
		width = $("#headerTable").width();
		$("#bodyBox").width(width);
		$('.col').css('width', 120);
		$('.col').css('max-width', 120);
		tableBody = document.getElementById("bodyBox");
		try{tableBody.scrollTop = tableBody.scrollHeight;}catch(err){}
	}
	else{
		$("#bodyBox",popList.tableBox.dom.document).height(($("#tableBox",popList.tableBox.dom.document).height()) - $(".table-header",popList.tableBox.dom.document).height());
		tableFix();
		tableBody = popList.tableBox.dom.document.getElementById("bodyBox",popList.tableBox.dom.document);
		tableBody.scrollTop = tableBody.scrollHeight;		
	}
}

/**
* tableFix() function
* Adjusts the dimenions of the table once it has
* been popped out.
**/
function tableFix(){
		var popWidth = $(popList.tableBox.dom.document).width() - 150;
		var head = popList.tableBox.dom.document.getElementById("headerTable");
		var top = head.rows[0];
		var cols = top.cells.length -1;
		var bestWidth = popWidth/cols;
		$('#bodyBox',popList.tableBox.dom.document).css('width','100%');
		$('#tableBox',popList.tableBox.dom.document).css('height','100%');
		$('.col',popList.tableBox.dom.document).css('width', bestWidth);
		$('.col',popList.tableBox.dom.document).css('max-width', bestWidth);
		if(bestWidth > 120){
			$('.col',popList.tableBox.dom.document).css('min-width', bestWidth);
		}
		else{
			$('.col',popList.tableBox.dom.document).css('min-width', 120);
			}
		$('.body-cell.col',popList.tableBox.dom.document).css('backgroundColor', 'white');
}

 /**
 * stepTable() function
 * Modify the table on each step through the code.
 * Allows for a variation of row and cell width based on the initilized table
 **/
function stepTable() {
	var win = popList.tableBox.dom.document;
	var head = win.getElementById("headerTable");
	var body = win.getElementById("bodyTable");
	var rows = body.rows.length;
	var newRow = body.insertRow(rows);
	var top = head.rows[0];
	var cols = top.cells.length;
	for(var i=0; i < cols; i++) {
		cell =newRow.insertCell(i);
		colvar = top.cells[i].innerHTML;
		cell.setAttribute("align","center");
		cell.setAttribute("class","body-cell col");	
		if(i==0){
			cell.setAttribute("class","body-cell");	
			cell.setAttribute("id","blueCell");
			cell.setAttribute("bgColor","#b3b3b3");
			cell.innerHTML = ('<svg>'+getSVG(curID)+'</svg>');	
		}
		else if(i==1){
			cell.setAttribute("bgColor","#ffffff");
			cell.innerHTML = (iteration);
		}
		else{
			cell.setAttribute("bgColor","#ffffff");
			cell.innerHTML = (varVal[colvar]);
			if(body.rows.length > 1){
				if(cell.innerHTML != body.rows[body.rows.length-2].cells[i].innerHTML){
					$(cell).effect("highlight", {color: 'rgb(3, 85, 122)'}, 3000);
				}
			}
		}
	}
	setTableBody();
}

 /**
 * resetTable() function
 * Removes the existing table and re-initilizes italitilizes it.
 * This is called by the parsing api
 **/
function resetTable(){
	var win = popList.tableBox.dom.document;
	 var box = win.getElementById("tableBox");
	 while(box.hasChildNodes()){
		 box.removeChild(box.firstChild);
	 }
	 initTable();
}

 /**
 * initTable() function
 * Builds the table on page and sets some initial values.
 * This is called by the resetTable function.
 **/
function initTable(){
	var win = popList.tableBox.dom.document;
	var popBut = win.createElement("div");
	popBut.setAttribute("onclick","popOut('tableBox')");
	popBut.setAttribute("id","tablePopOut");
	var popIcon = win.createElement("span");
	popIcon.setAttribute("class","glyphicon glyphicon-share");
	popBut.appendChild(popIcon);
	if(popList.tableBox.popped == true){
		popBut.setAttribute("style","display:none");
	}
	win.getElementById("tableBox").appendChild(popBut);	
	var keys = Object.keys(varVal);
	var varNum = keys.length;
	var headBox = win.createElement("div");
	headBox.setAttribute("class","table-header");
	var headTable = win.createElement("table");
	headTable.setAttribute("id","headerTable");
	var head = win.createElement("thead");
	head.setAttribute("id","tableHead");
	var row = head.insertRow(0);
	var cellDef = win.createElement("th");
	cellDef.innerHTML = ("Block");
	cellDef.setAttribute("class","header-cell");
	cellDef.setAttribute("id","blueCell");
	cellDef.setAttribute("align","center");
	row.appendChild(cellDef);
	var cellIt = win.createElement("th");
	cellIt.innerHTML = ("Step");
	cellIt.setAttribute("class","header-cell col");
	cellIt.setAttribute("id","itCell");
	cellIt.setAttribute("align","center");
	row.appendChild(cellIt);
	for(var i=0; i < varNum; i++) {
		cell = win.createElement("th");
		cell.innerHTML = (keys[i]);
		cell.setAttribute("class","header-cell col");
		cell.setAttribute("bgColor","#404040");
		cell.setAttribute("id","blockCell");
		cell.setAttribute("align","center");
		row.appendChild(cell);
	}
	var bodyBox = win.createElement("div");
	bodyBox.setAttribute("class","table-body");
	bodyBox.setAttribute("id","bodyBox");
	var bodyTable = win.createElement("table");
	bodyTable.setAttribute("id","bodyTable");
	var tableBody = win.createElement("tbody");
	tableBody.setAttribute("id","tableBody");
	bodyTable.appendChild(tableBody);
	bodyBox.appendChild(bodyTable);
	headTable.appendChild(head);	
	headBox.appendChild(headTable);
	win.getElementById("tableBox").appendChild(headBox);
	win.getElementById("tableBox").appendChild(bodyBox);
}

/**
* printTable() function
* Prepares a printable copy if the table in a 
* new window.
**/
function printTable(){
	var divToPrint=document.getElementById("tableBox");
	newWin= window.open("");
	newWin.document.write('<html><head><meta charset="utf-8"><!--Page title--><title>Algorithm Visualizer</title><!--Icon for the page--><link rel="shortcut icon" href="favicon.ico" /><!--Ensure compatability with mobile devices--><meta name="viewport" content="width=device-width, initial-scale=1"><!-- Bootstrap CSS --><link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"><link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css"><!-- jQuery library --><script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script><!--jQuery UI library--><link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css"><script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script><!-- Bootstrap JavaScript --><script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script><!---Import the blockly resources---><script src="acorn.js"></script><script src="interpreter.js"></script><!--Import the Visja reasources--><script type="text/javascript" src="Vis/exampleUtil.js"></script><script type="text/javascript" src="Vis/vis-network.min.js"></script><script type="text/javascript" src="Vis/vis.js"></script><link href="Vis/vis.css" rel="stylesheet" type="text/css" />		<!--Add JS file for the graph creation functions--><script src="graphFunctions.js"></script>	<script src="googleAnalytics.js"></script><!--Add the JS file for the console functions--><script src="consoleFunctions.js"></script>	<!--Add custom CSS--><link rel="stylesheet" href="style.css" ><!--Add JS file for the table creation functions--><script src="tableFunctions.js"></script><!--Add JS file for the pop-out functions--><script src="popFunctions.js"></script><!--Add JS file for the list functions--><script src="listFunctions.js"></script><script>$(window).resize(function(){try{scaleTab();}catch(err){}});</script></head><body>');

	newWin.document.write(divToPrint.outerHTML);
	newWin.document.write('</body></html>');
	setTimeout(function(){newWin.print();newWin.close();},10);
}

/* *
* TablePop() function
* Removes the table from the page and places it in a seperate browser window.
**/
function tablePop(){
	winPop('table');
	if($('#inPic').length != 0){
		parseCode();
	}
	var thatSide = document.getElementById("tableBox");
	tabWin = window.open("", "littleWindow", "location=no,width=500,height=500");
	var headData = '<html><head><meta charset="utf-8"><title>Algorithm Visualizer</title><link rel="shortcut icon" href="favicon.ico" /><meta name="viewport" content="width=device-width, initial-scale=1"><link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"><link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css"><script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script><link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css"><script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script><script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script><link rel="stylesheet" href="style.css" ><script src="tableFunctions.js"></script><script>$(window).resize(function(){try{scaleTab();}catch(err){alert(err);}});</script></head><body>';
	tabWin.document.write (headData);
	tabWin.document.write(thatSide.outerHTML);
	tabWin.document.write('</body></html>');
	tablePopped = true;
	tabWin.onbeforeunload = restoreTab;
	fixTab = setInterval(function(){scaleTab();},100);
}		
/**
* scaleTab() function
* Changes the css for the elements of the pop-out table
**/
function scaleTab(){
	var winHigh = $(window,popList.tableBox.dom).height()
	$('#tableBox',popList.tableBox.dom.document).css('height', '100%');
	$('#tablePopOut',popList.tableBox.dom.document).css('display', 'none');
	tableFix();
	clearInterval(fixTab);
	setTableBody();
}

/**
* restoreTab() function
* Places the table back in the main page and 
* restores the sizing.
**/
function restoreTab(){
	$('#tablePopOut').css('display', 'block');
	$('.body-cell.col').css('backgroundColor', 'white');
	setTableBody();	
	setTimeout(function(){setTableBody();},200);
	return null;
}

/**
* getSVG() function 
* Getsthe SVG of a given block.
* @arg the id of the block to be converted to an SVG
* @return the generated SVG image
**/
function getSVG(id){
	var l = workspace.getBlockById(id);
	thisBlockSvg = l.svgGroup_.outerHTML;
	if(l.getNextBlock() != null){
		nxtBlockSvg = l.getNextBlock().svgGroup_.outerHTML;
		thisBlockSvg = thisBlockSvg.replace(nxtBlockSvg,"");
	}
	return thisBlockSvg;
}